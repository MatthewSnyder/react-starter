const { name } = require('./package.json')

module.exports = {
	apps: [
		{
			name,
			script: 'app.js',

			// Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
			instances: 0,
			autorestart: true,
			watch: false,
			max_memory_restart: '1G',
			exp_backoff_restart_delay: 100,
			out_file: './logs/pm2-out.log',
			error_file: './logs/pm2-error.log',
			env: {
				NODE_ENV: 'development',
			},
			env_production: {
				NODE_ENV: 'production',
			},
		},
	],
}
