module.exports = {
	schema: {
		errors: {
			invalidInput: 'Error validating input. Please try again later.',
		},
	},

	user: {
		confirmations: {
			login: 'Successfully logged in!',
			logout: 'Successfully logged out!',
			confirmEmail: 'Success! Please check your email to verify your account.',
			updatedEmail: 'Successfully updated email.',
			emailVerified: 'Email successfully verified!',
			updatedPassword: 'Successfully updated password.',
			addedPassword: 'Successfully added password.',
			passwordResetEmail:
				'Please check your email to reset your password. The link is valid for 24 hours.',
			passwordResetRequest:
				'Valid link. Please enter and confirm your new password.',
			passwordReset: 'Successfully reset password.',
		},

		notices: {},

		errors: {
			loggingIn: 'Error logging in. Please try again later.',
			loggingOut: 'Error logging out. Please try again later.',
			registering: 'Error registering. Please try again later.',
			socialVerification:
				'Error verifying your social account. Please try again later.',
			verifyingEmail: 'Error verifying your email. Please try again later.',
			resettingPassword:
				'That email does not appear to exist, please try again.',
			passwordResetExpired: 'Reset link expired, please request a new one.',
			notLoggedIn: 'You need to be logged in to perform that action.',
			invalidLogin: 'Invalid credentials.',
			invalidEmail: (email) => `${email} is not a valid address.`,
			emailNotVerified: (email) => `${email} has not been verified.`,
			verifyingEmailTooSoon: (waitTime) =>
				`You have requested another verification email too soon. Please wait ${waitTime} before trying again.`,
			passwordResetRequestTooSoon: (waitTime) =>
				`You have requested another password reset email too soon. Please wait ${waitTime} before trying again.`,
			invalidPassword: 'Incorrect password entered. Please try again.',
			noPassword:
				'You logged in with a social account and have no password associated with your account. Please try logging in with a social account then enter a password.',
			invalidAuthToken: 'Invalid auth token',
			userExists: 'A user with that email already exists.',
			userDoesNotExist: 'That user does not exist.',
			doesNotExist: 'Object does not exist.',
			session: 'Error validating session. Please login again.',
			permission: 'You do not have permission to perform that action.',
			twitter:
				"For your account's security you cannot login with Twitter if your email is already verified since Twitter doesn't provide a capability for retrieving the email associated with the account.",
			updatingEmail: 'Error updating email. Please try again later.',
			updatingPassword: 'Error updating password. Please try again later.',
			samePassword: 'Cannot use current password as the new password.',
			invalidInfo: 'Invalid information supplied!',
		},
	},

	react: {
		generalError:
			'A serious error has occurred while trying to render the page. Please try again later or contact us for help.',
	},

	generalError: 'An error occurred. Please try again later.',
}
