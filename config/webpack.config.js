/* eslint-disable security/detect-unsafe-regex */
/* eslint-disable security/detect-object-injection */
const { existsSync, mkdirSync, writeFileSync } = require('fs')
const path = require('path')

const { join } = path
const webpack = require('webpack')
const ESLintPlugin = require('eslint-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const LoadablePlugin = require('@loadable/webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const ProgressBarPlugin = require('progress-bar-webpack-plugin')
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')

const nodeExternals = require('webpack-node-externals')
const progressBar = require('progress')

const root = join(__dirname, '..')
const src = join(root, 'src')
const modules = join(root, 'node_modules')
const clientDest = join(root, 'build-client')
const clientPublicPath = '/build-client/'
const serverDest = join(root, 'build-server')
const serverPublicPath = '/build-server/'
const testDest = join(root, 'build-test')
const testPublicPath = '/build-test/'

const NODE_ENV = process.env.NODE_ENV || 'production'
const IS_PROD = NODE_ENV === 'production'
const IS_DEV = NODE_ENV === 'development'
const IS_TEST = NODE_ENV === 'test'

// const minFileSize = 10000

const dotenv = require('dotenv')

dotenv.config({ path: join(root, '.env') })

dotenv.config({
	path: join(root, 'config', `${NODE_ENV}.env`),
	silent: true,
})

const envVariables = process.env

const defines = Object.keys(envVariables).reduce(
	(memo, key) => {
		const val = JSON.stringify(envVariables[key])
		return { ...memo, [`__${key.toUpperCase()}__`]: val }
	},
	{
		__NODE_ENV__: JSON.stringify(NODE_ENV),
		__IS_PROD__: JSON.stringify(IS_PROD),
		__IS_DEV__: JSON.stringify(IS_DEV),
		__SERVER_PORT__: JSON.stringify(envVariables.PORT),
		'process.env.NODE_ENV': JSON.stringify(NODE_ENV),
	}
)

/* eslint-disable no-underscore-dangle */
if (typeof defines.__FB_APP_ID__ === 'undefined') defines.__FB_APP_ID__ = false

if (typeof defines.__GOOGLE_CLIENT_ID__ === 'undefined')
	defines.__GOOGLE_CLIENT_ID__ = false
/* eslint-enable no-underscore-dangle */

const clientDefines = { ...defines, __IS_CLIENT__: true, __IS_SERVER__: false }

const serverDefines = {
	...defines,
	__IS_CLIENT__: false,
	__IS_SERVER__: true,
	__REDIS_APOLLO_HOST__: JSON.stringify(envVariables.REDIS_APOLLO_HOST),
	__REDIS_APOLLO_PORT__: JSON.stringify(envVariables.REDIS_APOLLO_PORT),
	__REDIS_APOLLO_DB__: JSON.stringify(envVariables.REDIS_APOLLO_DB) || '1',
	'navigator.userAgent': "''",
	'window.location': {},
	window: {},
}

const cssLoader = {
	loader: 'css-loader',
	options: { sourceMap: IS_DEV },
}

const clientFinalCssLoader = IS_DEV
	? 'style-loader'
	: MiniCssExtractPlugin.loader

const clientCssModuleLoader = {
	loader: 'css-loader',
	options: {
		modules: { localIdentName: '[name]__[local]___[hash:base64:5]' },
		sourceMap: true,
	},
}

const serverCssModuleLoader = {
	loader: 'css-loader',
	options: {
		modules: {
			localIdentName: '[name]__[local]___[hash:base64:5]',
			exportOnlyLocals: true,
		},
	},
}

const serverCssLoader = {
	loader: 'css-loader',
	options: {
		modules: { exportOnlyLocals: true },
	},
}

const lessLoader = {
	loader: 'less-loader',
	options: { sourceMap: IS_DEV },
}

const postCssLoader = {
	loader: 'postcss-loader',
	options: {
		sourceMap: IS_DEV,
	},
}

const common = {
	mode: IS_PROD ? 'production' : 'development',
	devtool: IS_PROD ? 'source-map' : 'eval-source-map',
	resolve: {
		alias: {
			constants: join(src, 'constants.js'),
			components: join(src, 'components'),
			containers: join(src, 'containers'),
			pages: join(src, 'pages'),
			images: join(src, 'images'),
			util: join(src, 'util'),
			'global-styles': join(src, 'global-styles'),
			root: src,
		},
		extensions: ['.js', '.min.js', '.jsx', '.json'],
	},
}

const clientConfig = {
	...common,
	name: 'client',
	target: 'web',
	context: src,
	entry: { main: [join(src, 'app.jsx')] },
	output: {
		path: clientDest,
		filename: IS_DEV ? '[name].bundle.js' : '[name]-[hash].bundle.js',
		chunkFilename: '[name]-[chunkhash].bundle.js',
		publicPath: clientPublicPath,
		hashFunction: 'xxhash64',
	},
	optimization: {
		mangleWasmImports: IS_PROD,
		minimizer: [
			new TerserPlugin({
				extractComments: IS_PROD,
			}),
		],
	},
	plugins: [
		new ESLintPlugin({ extensions: ['js', 'jsx'] }),
		new CleanWebpackPlugin(),
		new webpack.DefinePlugin(clientDefines),
		new webpack.LoaderOptionsPlugin({
			debug: IS_DEV,
			minimize: IS_PROD,
		}),
		new LoadablePlugin(),
		new MiniCssExtractPlugin({
			filename: IS_DEV ? '[name].css' : '[name]-[hash].css',
			ignoreOrder: true,
		}),
		/* new webpack.optimize.MinChunkSizePlugin({
			minChunkSize: minFileSize
		}), */
		/* new HtmlWebpackPlugin({
			cache: IS_PROD,
			template: join(src, 'index.pug'),
			inject: false,
			minify: IS_DEV ? false : {
				removeComments: IS_PROD,
				collapseWhitespace: IS_PROD
			}
		}) */
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx|babel)$/,
				exclude: [modules],
				use: [
					'thread-loader',
					{
						loader: 'babel-loader',
						options: {
							cacheDirectory: true,
							plugins: [
								IS_DEV && require.resolve('react-refresh/babel'),
							].filter(Boolean),
						},
					},
				],
			},
			{
				test: /\.(otf|eot|svg|ttf|woff2?)(\?\S*)?$/,
				type: 'asset/inline',
			},
			{
				test: /\.(jpe?g|png|gif)$/,
				type: 'asset/inline',
			},
			{
				test: /\.module\.css$/,
				use: [clientFinalCssLoader, clientCssModuleLoader, postCssLoader],
			},
			{
				test: /^((?!module).)*\.less$/,
				use: [clientFinalCssLoader, cssLoader, postCssLoader, lessLoader],
			},
			{
				test: /\.module\.less$/,
				use: [
					clientFinalCssLoader,
					clientCssModuleLoader,
					postCssLoader,
					lessLoader,
				],
			},
			{
				test: /\.css$/,
				use: [clientFinalCssLoader, cssLoader, postCssLoader],
			},
			{
				test: /\.pug/,
				use: 'simple-pug-loader',
			},
			{
				test: /\.html/,
				loader: 'html-loader',
				options: { globals: clientDefines },
			},
		],
	},
	watchOptions:
		envVariables.DOCKER === '1'
			? {
					aggregateTimeout: 300,
					ignored: /node_modules/,
					poll: true,
			  }
			: { ignored: /node_modules/ },
	devServer: {
		contentBase: clientDest,
		compress: true,
		hot: true,
		overlay: {
			warnings: true,
			errors: true,
		},
		stats: {
			chunks: false,
		},
		historyApiFallback: true,
		// proxy: { '*': `http://localhost:${envVariables.PORT}` },
		devMiddleware: {
			publicPath: clientPublicPath,
			stats: { chunks: 'normal' },
			writeToDisk: true,
		},
	},
}

let serverConfig = {
	...common,
	name: 'server',
	context: src,
	target: 'node',
	node: { __dirname: false },
	entry: join(src, 'server.jsx'),
	output: {
		libraryTarget: 'commonjs2',
		path: serverDest,
		filename: 'server.js',
		publicPath: serverPublicPath,
		hashFunction: 'xxhash64',
	},
	plugins: [
		new ESLintPlugin({ extensions: ['js', 'jsx'] }),
		new CleanWebpackPlugin(),
		new webpack.DefinePlugin(serverDefines),
		new webpack.LoaderOptionsPlugin({
			debug: IS_DEV,
			minimize: IS_PROD,
		}),
		new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 }),
	],
	externals: [
		nodeExternals({
			allowlist: [
				'webpack-flush-chunks',
				'webpack/hot/poll',
				/\.(?!(?:jsx?|json)$).{1,5}$/i,
			],
		}),
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx|babel)$/,
				exclude: [modules],
				use: [
					'thread-loader',
					{
						loader: 'babel-loader',
						options: { cacheDirectory: true },
					},
				],
			},
			{
				test: /\.(otf|eot|svg|ttf|woff2?)(\?\S*)?$/,
				type: 'asset/inline',
			},
			{
				test: /\.(jpe?g|png|gif)$/,
				type: 'asset/inline',
			},
			{
				test: /\.module\.css$/,
				use: [serverCssModuleLoader, postCssLoader],
			},
			{
				test: /^((?!module).)*\.less$/,
				use: [serverCssLoader, postCssLoader, 'less-loader'],
			},
			{
				test: /\.module\.less$/,
				use: [serverCssModuleLoader, postCssLoader, 'less-loader'],
			},
			{
				test: /\.css$/,
				use: [serverCssLoader, postCssLoader],
			},
			{
				test: /\.pug/,
				use: 'simple-pug-loader',
			},
			{
				test: /\.html/,
				loader: 'html',
			},
		],
	},
	watchOptions:
		envVariables.DOCKER === '1'
			? {
					aggregateTimeout: 300,
					ignored: /node_modules/,
					poll: true,
			  }
			: { ignored: /node_modules/ },
}

if (!IS_TEST) {
	clientConfig.plugins.push(
		new ProgressBarPlugin((percentage) => progressBar.update(percentage))
	)
	serverConfig.plugins.push(
		new ProgressBarPlugin((percentage) => progressBar.update(percentage))
	)
} else {
	serverConfig.output.path = testDest
	clientConfig.output.path = serverConfig.output.path
	serverConfig.output.publicPath = testPublicPath
	clientConfig.output.publicPath = serverConfig.output.publicPath

	serverConfig = {
		resolve: {
			fallback: { fs: false },
		},
	}
	clientConfig.resolve = serverConfig.resolve

	Object.assign(serverConfig.externals, clientConfig.externals)
}

if (IS_DEV) {
	clientConfig.entry.main.unshift(
		'webpack/hot/dev-server',
		'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=false'
	)
	clientConfig.plugins.push(new webpack.HotModuleReplacementPlugin())
	clientConfig.plugins.push(new ReactRefreshWebpackPlugin())
} else {
	/* Doesn't seem to help as much as promised ;(
	https://hackernoon.com/the-100-correct-way-to-split-your-chunks-with-webpack-f8a9df5b7758
	clientConfig.optimization['runtimeChunk'] = 'single'
	clientConfig.optimization['splitChunks'] = {
		chunks: 'all',
		maxInitialRequests: Infinity,
		minSize: 0,
		cacheGroups: {
			defaultVendors: {
				test: /[\\/]node_modules[\\/]/,
				name(module) {
					// get the name. E.g. node_modules/packageName/not/this/part.js
					// or node_modules/packageName
					const packageName = module.context.match(
						/[\\/]node_modules[\\/](.*?)([\\/]|$)/
					)[1]

					// npm package names are URL-safe, but some servers don't like @ symbols
					return `npm.${packageName.replace('@', '')}`
				},
			},
		},
	} */
}

const config = [clientConfig, serverConfig]

if (IS_PROD) {
	const configPath = join(root, 'build-info')

	if (!existsSync(configPath)) mkdirSync(configPath)

	writeFileSync(
		join(configPath, 'webpack-prod-config.json'),
		JSON.stringify(config)
	)
}

module.exports = config
