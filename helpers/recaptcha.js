const request = require('request')
const util = require('util')
const mongo = require('../db')
const logger = require('./logger')

module.exports.verify = (secret, response, ip, callback) => {
	request.post(
		'https://www.google.com/recaptcha/api/siteverify',
		{
			form: {
				secret,
				response,
				remoteip: ip,
			},
		},
		function receive(error, httpResponse, body) {
			const bodyResponse = JSON.parse(body)
			let cbError = error

			if (!error && !bodyResponse.success) {
				logger.log(util.inspect(bodyResponse))
				cbError = bodyResponse
			}

			callback(cbError)
		}
	)
}

module.exports.error = (req, error) => {
	const captchaErrors = mongo.collection('captcha_errors')

	let captchaError

	captchaError = 'Captcha not verified.'

	captchaError += `\nIP: ${req.ip}`

	captchaErrors.insert(
		{
			error,
			date: new Date(),
		},
		{},
		(insertError, records) => {
			if (insertError) {
				logger.error(insertError)
				throw insertError
			} else logger.log(`Captcha Error added as ${captchaError}`, records)
		}
	)
}
