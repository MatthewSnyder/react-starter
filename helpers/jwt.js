const jwt = require('jsonwebtoken')
const logger = require('./logger')
const vars = require('./vars')

const { SESSION_EXPIRE } = vars

const secret = process.env.JWT_SECRET

const algorithm = 'HS512'

const defaultOptions = {
	algorithm,
	expiresIn: SESSION_EXPIRE / 1000,
}

module.exports.sign = (payload, opts = {}) => {
	try {
		const options = Object.assign(opts, defaultOptions)
		return jwt.sign(payload, secret, options)
	} catch (error) {
		logger.error(error)
		throw error
	}
}

module.exports.verify = (token) => jwt.verify(token, secret, { algorithm })
