const { createLogger, transports, format } = require('winston')

const { combine, timestamp, errors, prettyPrint } = format
const fs = require('fs')
const path = require('path')

const dir = path.resolve(__dirname, '../logs')

// eslint-disable-next-line security/detect-non-literal-fs-filename
if (!fs.existsSync(dir)) fs.mkdirSync(dir)

const logger = createLogger({
	format: combine(timestamp(), errors({ stack: true }), prettyPrint()),
	transports: [
		new transports.Console({ handleExceptions: true }),
		new transports.File({
			filename: path.join(dir, 'out.log'),
			level: 'info',
		}),
		new transports.File({
			filename: path.join(dir, 'error.log'),
			level: 'error',
			handleExceptions: true,
		}),
	],
})

logger.exceptions.handle(
	new transports.File({ filename: path.join(dir, 'exceptions.log') })
)

module.exports = logger
