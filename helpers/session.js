const express = require('express')
const session = require('express-session')
const MemoryStore = require('memorystore')(session)
const redis = require('redis')
const RedisStore = require('connect-redis')(session)

const logger = require('./logger')
const mongo = require('./db')
const { SESSION_EXPIRE } = require('./vars')

const messages = require('../config/messages')

const {
	jwtLogin,
	userExists,
	performLogin,
	logout,
} = require('../schema/User/util')

const {
	NODE_ENV,
	DOMAIN,
	SESSION_SECRET,
	REDIS_SESSION_HOST,
	REDIS_SESSION_PORT,
	REDIS_SESSION_DB,
	REDIS_SESSION_PASSWORD,
} = process.env

const memoryStoreConfig = {
	checkPeriod: SESSION_EXPIRE / 2000,
	expires: SESSION_EXPIRE / 1000,
}

const router = express.Router()

const getSessionStore = () =>
	new Promise((resolve, reject) => {
		if (REDIS_SESSION_HOST && REDIS_SESSION_PORT) {
			const redisClient = redis.createClient({
				host: REDIS_SESSION_HOST,
				port: parseInt(REDIS_SESSION_PORT, 10) || 6379,
				db: parseInt(REDIS_SESSION_DB, 10) || 0,
				password: REDIS_SESSION_PASSWORD,
			})

			redisClient.on('error', (error) => {
				if (error.code === 'ECONNREFUSED') {
					logger.info(
						'Redis Client connection refused error, falling back to memorystore for session'
					)
					redisClient.quit()
					resolve(new MemoryStore(memoryStoreConfig))
				} else {
					logger.error(error)
					reject(error)
				}
			})

			redisClient.on('ready', () => {
				redisClient.get('test', (error) => {
					if (error) {
						logger.error(error)
						resolve(new MemoryStore(memoryStoreConfig))
					} else {
						redisClient.on('error', logger.error)
						logger.info('Session Redis Ready')
						resolve(new RedisStore({ client: redisClient }))
					}
				})
			})
		} else {
			logger.info(
				'No Redis Host and/or port specified, using memory store for session'
			)

			resolve(new MemoryStore(memoryStoreConfig))
		}
	})

async function sessionError(error, req, res, next) {
	logger.error(error)

	if (req.session) {
		req.session.errors.push(messages.user.errors.session)
		await logout(req, res)
	}

	if (req.path !== '/') res.redirect('/')
	else next()
}

module.exports = (async () => {
	try {
		const SessionStore = await getSessionStore()

		router.use(
			session({
				store: SessionStore,
				cookie: {
					maxAge: SESSION_EXPIRE,
					secure: NODE_ENV === 'production',
				},
				name: DOMAIN,
				secret: SESSION_SECRET || 'secret',
				resave: false,
				saveUninitialized: false,
				rolling: true,
			})
		)
	} catch (error) {
		logger.error(error)
		throw error
	}

	router.use(async (req, res, next) => {
		if (req.session) {
			try {
				if (!Array.isArray(req.session.confirmations)) {
					req.session.confirmations = []
				}

				if (!Array.isArray(req.session.information)) {
					req.session.information = []
				}

				if (!Array.isArray(req.session.errors)) {
					req.session.errors = []
				}

				if (req.session.user) {
					const user = await userExists(
						{ email: req.session.user.email },
						{ mongo }
					)

					if (user) {
						await performLogin(user, { mongo, req, res })
						next()
					} else await sessionError("User doesn't exist", req, res, next)
				} else if (req.headers.authorization) {
					let token = req.headers.authorization

					if (token.startsWith('Bearer ')) {
						// Remove Bearer from string
						token = token.slice(7, token.length)
					}

					await jwtLogin(token, { mongo, req, res })
					next()
				} else if (req.cookies && req.cookies.user) {
					await jwtLogin(req.cookies.user, { mongo, req, res })
					next()
				} else if (req.cookies && req.cookies.jwt) {
					await jwtLogin(req.cookies.jwt, { mongo, req, res })
					next()
				} else next()
			} catch (error) {
				await sessionError(error, req, res, next)
			}
		} else next()
	})

	return router
})()
