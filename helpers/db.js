const { MongoClient } = require('mongodb')
const logger = require('./logger')
const InitSchema = require('../schema/init')

const { env } = process

let auth = {}

if (env.DB_USER && env.DB_PASSWORD)
	auth = {
		username: env.DB_USER,
		password: env.DB_PASSWORD,
	}

let db

if (
	typeof env.DB_HOST === 'undefined' ||
	typeof env.DB_PORT === 'undefined' ||
	typeof env.DB_DATABASE === 'undefined'
)
	logger.error(
		'Missing one or more required environment variables: DB_HOST, DB_PORT, DB_DATABASE'
	)

const url = encodeURI(
	`mongodb://${env.DB_HOST}:${env.DB_PORT}/${env.DB_DATABASE}`
)

module.exports.collection = (collection) =>
	new Promise((resolve, reject) => {
		if (typeof db === 'undefined')
			return MongoClient.connect(
				url,
				{
					maxPoolSize: env.DB_POOL_SIZE,
					useNewUrlParser: true,
					// useUnifiedTopology: true,
					auth,
				},
				(error, client) => {
					if (error) {
						reject(error)
						return
					}

					db = client.db(env.DB_DATABASE)
					logger.info(`Successfully connected to database: ${url}`)
					InitSchema(db).then(() => {
						resolve(db.collection(collection))
					})
				}
			)

		return resolve(db.collection(collection))
	}).catch((error) => logger.error(error))
