const mailer = require('./mailer')
const messages = require('../config/messages')

function ApiError(message, options = {}) {
	Error.call(this)
	Error.captureStackTrace(this)
	this.name = 'ApiError'
	this.message = message || messages.generalError
	this.options = options

	if (options.report) mailer.reportError(this.message, this)
}

ApiError.prototype = Object.create(Error.prototype)
ApiError.prototype.constructor = ApiError

module.exports = ApiError
