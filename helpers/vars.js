const ms = require('ms')

const { env } = process

module.exports.SESSION_EXPIRE = env.SESSION_EXPIRE
	? ms(env.SESSION_EXPIRE)
	: ms('30 days')
