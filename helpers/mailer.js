const nodemailer = require('nodemailer')
const smtpTransport = require('nodemailer-smtp-transport')
const { join } = require('path')
const fs = require('fs')
const pug = require('pug')
const { promisify } = require('util')
const logger = require('./logger')

// eslint-disable-next-line security/detect-non-literal-fs-filename
const readFileAsync = promisify(fs.readFile)

const errorContactEmail = process.env.ERROR_CONTACT_EMAIL
const domain = process.env.DOMAIN

const {
	MAIL_HOST,
	MAIL_PORT,
	MAIL_SECURE,
	MAIL_USER,
	MAIL_PASS,
	REQUIRE_EMAIL_AUTHENTICATION,
} = process.env

const areMailEnvSet = MAIL_HOST && MAIL_PORT
const shouldMailServerBeAvailable = REQUIRE_EMAIL_AUTHENTICATION === 'true'

if (shouldMailServerBeAvailable && !areMailEnvSet)
	logger.error('Missing mail host and port!')

let transporter = () => {
	throw new Error('Missing mail host and port!')
}

let sendMail = transporter

if (areMailEnvSet) {
	const auth = {}

	if (MAIL_USER || MAIL_PASS)
		auth.auth = {
			user: MAIL_USER,
			password: MAIL_PASS,
		}

	transporter = nodemailer.createTransport(
		smtpTransport(
			{
				host: MAIL_HOST || 'localhost',
				port: MAIL_PORT || '465',
				secure: MAIL_SECURE || false,
				...auth,
			},
			{
				from: `noreply@${domain}`,
			}
		)
	)

	transporter.verify((error, success) => {
		if (error) {
			logger.error('Error connecting to mail server:')
			logger.error(error)
		} else if (success) {
			logger.info('Successfully connected to mail server!')
		}
	})

	sendMail = (_options) =>
		new Promise((resolve, reject) => {
			const options = { ..._options }

			transporter.sendMail(options, (error, message) => {
				if (error) return reject(error)

				if (message) {
					const { messageId, response } = message

					logger.info(`Message ${messageId} sent: ${response}`)
					return resolve(response)
				}

				return reject()
			})
		}).catch((error) => {
			logger.error(error)
			throw error
		})
}

module.exports.sendMail = sendMail

module.exports.sendMailTemplate = async (
	template,
	templateOptions,
	_options
) => {
	try {
		const data = await readFileAsync(
			join(__dirname, `../email-templates/${template}`)
		)

		const pugTemplate = pug.compile(data, {
			cache: false,
		})

		const options = {
			..._options,
			html: pugTemplate({
				...templateOptions,
				domain,
			}),
		}

		if (!options.from) options.from = `noreply@${domain}`

		return sendMail(options)
	} catch (error) {
		logger.error(error)
		throw error
	}
}

module.exports.reportError = async (error) => {
	try {
		logger.error(error)

		return sendMail({
			from: `noreply@${domain}`,
			to: errorContactEmail,
			subject: `${domain} Error`,
			text:
				typeof error === 'object'
					? `${error.message}\n\n${error.stack}`
					: error,
		})
	} catch (anotherError) {
		logger.error(anotherError)
		throw anotherError
	}
}

module.exports.userReportError = async (user, message) => {
	try {
		const from = user ? user.email : `noreply@${domain}`

		return sendMail({
			from,
			to: errorContactEmail,
			subject: 'Error',
			html: (user ? `User ID: ${user.id}<br/><br/>` : '') + message,
		})
	} catch (error) {
		logger.error(error)
		throw error
	}
}

module.exports.getTransporter = () => {
	return transporter
}
