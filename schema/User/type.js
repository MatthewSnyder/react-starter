module.exports = `
	type User implements Node {
		id: ID!
		facebook_id: ID
		google_id: ID
		first_name: String
		last_name: String
		username: String
		email: String!
		password: String
		has_password: Boolean
		auth_token: String!
		auth_token_updated: DateTime
		verifying_email: String
		email_verification: String
		email_verification_request_date: DateTime
		email_verified: Boolean
		password_reset_verification: String
		password_reset_request_date: DateTime
		created_at: DateTime!
		updated_at: DateTime
		roles: [String!]
		settings: UserSettings
	}

	type UserSettings {
		subscribed: Boolean
	}

	type LogInPayload {
		jwt: String!
		user: User!
		message: String!
	}

	type RegistrationPayload {
		message: String!
		jwt: String
		user: User
	}

	type UserConnection implements Connection {
		edges: [UserEdge!]
		pageInfo: PageInfo
	}

	type UserEdge implements Edge {
		node: User!
		cursor: Cursor!
	}
`
