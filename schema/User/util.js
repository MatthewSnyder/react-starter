const bcrypt = require('bcryptjs')
const crypto = require('crypto')
const moment = require('moment')
const validator = require('validator')
const ms = require('ms')

const { authTokenLength } = require('./vars')
const { prepareNode, errorHandler } = require('../util')

const logger = require('../../helpers/logger')
const mailer = require('../../helpers/mailer')
const ApiError = require('../../helpers/api-error')
const { sign: jwtSign, verify: jwtVerify } = require('../../helpers/jwt')

const messages = require('../../config/messages')
const apiUrlConfig = require('../../config/api-url.conf')

const { userCollectionName, requireEmailAuthentication } = require('./vars')

const prepareUser = (user) => prepareNode(user)

module.exports.prepareUser = prepareUser

module.exports.hashPassword = (password) => {
	try {
		return bcrypt.hashSync(password, 10)
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

const randomHexValue = (len) =>
	crypto
		.randomBytes(Math.ceil(len / 2))
		.toString('hex') // convert to hexadecimal format
		.slice(0, len) // return required number of characters

module.exports.randomHexValue = randomHexValue

const authenticate = async (dirtyEmail, password, { mongo, userLoader }) => {
	try {
		const email = dirtyEmail.toLowerCase()

		const collection = await mongo.collection(userCollectionName)
		const user = await collection.findOne({ email })

		if (!user) throw new ApiError(messages.user.errors.invalidLogin) // Don't tell them there's no user vs invalidLogin!

		userLoader.prime(user._id, user)

		if (requireEmailAuthentication && !user.email_verified)
			throw new ApiError(messages.user.errors.emailNotVerified(email))

		if (!user.password) throw new ApiError(messages.user.errors.noPassword)

		if (!bcrypt.compareSync(password, user.password))
			throw new ApiError(messages.user.errors.invalidLogin)

		return user
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

module.exports.authenticate = authenticate

const getAuthToken = async (user, { mongo, userLoader }) => {
	try {
		const collection = await mongo.collection(userCollectionName)

		if (
			user.auth_token &&
			user.auth_token.length === authTokenLength &&
			user.auth_token_updated &&
			moment(user.auth_token_updated)
				.add(ms(process.env.SESSION_EXPIRE), 'milliseconds')
				.isAfter(Date.now())
		) {
			const { ok, value: updatedUser } = await collection.findOneAndUpdate(
				{ email: user.email },
				{ $set: { auth_token_updated: Date.now(), updated_at: Date.now() } },
				{ returnDocument: 'after' }
			)

			if (!ok) throw new ApiError(messages.generalError, { report: true })

			if (userLoader) userLoader.prime(updatedUser._id, updatedUser)

			return updatedUser
		}

		const authToken = randomHexValue(authTokenLength)

		const { ok, value: updatedUser } = await collection.findOneAndUpdate(
			{ email: user.email },
			{
				$set: {
					auth_token: authToken,
					auth_token_updated: Date.now(),
					updated_at: Date.now(),
				},
			},
			{ returnDocument: 'after' }
		)

		if (!ok) throw new ApiError(messages.generalError, { report: true })

		if (userLoader) userLoader.prime(updatedUser._id, updatedUser)

		return updatedUser
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

const performLogin = async (user, context) => {
	try {
		const { req, res } = context
		const updatedUser = prepareUser(await getAuthToken(user, context))
		req.session.user = updatedUser

		const jwt = jwtSign(updatedUser)
		res.cookie('jwt', jwt, { maxAge: ms(process.env.SESSION_EXPIRE) })
		return { user: updatedUser, jwt }
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

module.exports.performLogin = performLogin

const logout = async (req, res) => {
	res.clearCookie('jwt')

	return new Promise((resolve) => {
		req.session.destroy((error) => {
			if (error) {
				throw errorHandler(error, messages.error.general)
			} else {
				resolve()
			}
		})
	})
}

module.exports.logout = logout

const jwtLogin = async (token, context) => {
	try {
		let decoded

		try {
			decoded = jwtVerify(token)
		} catch (error) {
			logout(context.req, context.res)
			return false
		}

		const email = decoded.email.toLowerCase()

		if (!validator.isEmail(email))
			throw new ApiError(messages.user.errors.invalidEmail(email))

		if (!decoded.auth_token || decoded.auth_token.length !== authTokenLength)
			throw new ApiError(messages.user.errors.invalidAuthToken)

		const collection = await context.mongo.collection(userCollectionName)

		const user = await collection.findOne({
			email,
			auth_token: decoded.auth_token,
		})

		if (!user || user.email !== email)
			throw new ApiError(messages.user.errors.invalidAuthToken)

		return performLogin(decoded, context)
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

module.exports.jwtLogin = jwtLogin

// Their social account and email have been verified

const socialLogin = async (email, query, context) => {
	try {
		const { mongo, userLoader } = context
		const collection = await mongo.collection(userCollectionName)
		const existingUser = await collection.findOne(query)

		if (existingUser) {
			if (!existingUser.email_verified && existingUser.email === email) {
				const { ok, value: updatedUser } = await collection.findOneAndUpdate(
					{ _id: existingUser._id },
					{
						$set: {
							email_verified: true,
							updated_at: Date.now(),
						},
					},
					{ returnDocument: 'after' }
				)

				if (!ok) throw new ApiError(messages.generalError, { report: true })

				userLoader.prime(updatedUser._id, updatedUser)

				return await performLogin(updatedUser, context)
			}
			return await performLogin(existingUser, context)
		}

		const existingEmailUser = await collection.findOne({ email })

		if (existingEmailUser) {
			const $set = {
				...query,
				email_verified: true,
				updated_at: Date.now(),
			}

			const { ok, value: updatedUser } = await collection.findOneAndUpdate(
				{ email },
				{ $set },
				{ returnDocument: 'after' }
			)

			userLoader.prime(updatedUser._id, updatedUser)

			if (!ok) throw new ApiError(messages.generalError, { report: true })

			return performLogin(updatedUser, context)
		}

		const newUser = {
			...query,
			email,
			email_verified: true,
			created_at: Date.now(),
			updated_at: Date.now(),
		}

		const { insertedId } = await collection.insertOne(newUser)

		if (!insertedId) throw new ApiError(messages.generalError, { report: true })

		newUser.id = insertedId
		userLoader.prime(newUser.id, newUser)

		logger.info({
			message: 'New user created',
			user: newUser,
		})

		return await performLogin(newUser, context)
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

module.exports.socialLogin = socialLogin

const userExists = async (userIdentifier, { mongo, userLoader }) => {
	try {
		const collection = await mongo.collection(userCollectionName)
		const user = await collection.findOne(userIdentifier)

		if (user && userLoader) userLoader.prime(user._id, user)

		return user
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

module.exports.userExists = userExists

// newEmail is optional, could be first time registering

const sendVerificationEmail = async (
	sanitizableOldEmail,
	{ req, mongo, userLoader },
	sanitizableNewEmail
) => {
	try {
		if (!sanitizableOldEmail || !validator.isEmail(sanitizableOldEmail))
			throw new ApiError(messages.user.errors.invalidEmail(sanitizableOldEmail))

		const oldEmail = sanitizableOldEmail.toLowerCase()
		let newEmail

		if (sanitizableNewEmail) {
			if (!validator.isEmail(sanitizableNewEmail))
				throw new ApiError(
					messages.user.errors.invalidEmail(sanitizableNewEmail)
				)

			newEmail = sanitizableNewEmail.toLowerCase()
		} else newEmail = oldEmail

		if (!validator.isEmail(newEmail))
			throw new ApiError(messages.user.errors.invalidEmail(newEmail))

		const collection = await mongo.collection(userCollectionName)

		const alreadyVerifying = await collection.findOne({
			email: oldEmail,
			verifying_email: newEmail,
		})

		if (alreadyVerifying) {
			const verificationRequestDate = moment(
				alreadyVerifying.email_verification_request_date
			).add(1, 'hour')

			if (moment().isSameOrBefore(verificationRequestDate)) {
				const waitPeriod = moment
					.duration(verificationRequestDate.diff(moment()))
					.humanize()

				throw new ApiError(
					messages.user.errors.verifyingEmailTooSoon(waitPeriod)
				)
			}
		}

		const emailHash = randomHexValue(20)

		const { ok, value: updatedUser } = await collection.findOneAndUpdate(
			{ email: oldEmail },
			{
				$set: {
					email_verification: emailHash,
					verifying_email: newEmail,
					email_verification_request_date: Date.now(),
					updated_at: Date.now(),
				},
			},
			{ returnDocument: 'after' }
		)

		if (!ok) throw new ApiError(messages.generalError, { report: true })

		userLoader.prime(updatedUser._id, updatedUser)

		const baseurl = `${req.protocol}://${process.env.DOMAIN}`

		return mailer.sendMailTemplate(
			'user/email-verification.pug',
			{
				url: `${
					baseurl +
					apiUrlConfig.api +
					apiUrlConfig.user +
					apiUrlConfig.emailVerification
				}/${encodeURIComponent(newEmail)}/${encodeURIComponent(emailHash)}`,
			},
			{
				to: newEmail,
				subject: 'Email Verification',
			}
		)
	} catch (error) {
		try {
			if (sanitizableOldEmail && validator.isEmail(sanitizableOldEmail)) {
				const oldEmail = sanitizableOldEmail.toLowerCase()
				let newEmail = sanitizableNewEmail

				if (!sanitizableNewEmail || validator.isEmail(sanitizableNewEmail)) {
					if (!sanitizableNewEmail) newEmail = oldEmail

					const collection = await mongo.collection(userCollectionName)

					const result = await collection.findOneAndUpdate(
						{ oldEmail },
						{
							$set: {
								verifying_email: '',
								email_verification_request_date: null,
								updated_at: Date.now(),
							},
						},
						{ returnDocument: 'after' }
					)

					if (!result.ok) {
						logger.error({
							message: 'Error rolling back verification email db changes',
							oldEmail,
							newEmail,
							result,
						})
					}
				}
			}
		} catch (anotherError) {
			throw errorHandler(anotherError, messages.generalError)
		}

		throw errorHandler(error, messages.generalError)
	}
}

module.exports.sendVerificationEmail = sendVerificationEmail

const verifyEmail = async ({ email: dirtyEmail, hash, mongo, req, res }) => {
	try {
		if (!dirtyEmail || !validator.isEmail(dirtyEmail))
			throw new ApiError(messages.user.errors.invalidEmail(dirtyEmail))

		const email = dirtyEmail.toLowerCase()

		const collection = await mongo.collection(userCollectionName)
		const user = await collection.findOne({ verifying_email: email })

		if (
			!user ||
			user.email_verification !== hash ||
			!user.email_verification_request_date
		) {
			throw new ApiError(messages.user.errors.invalidInfo)
		}

		if (
			moment(user.email_verification_request_date)
				.add(1, 'days')
				.isSameOrBefore(Date.now())
		) {
			throw new ApiError(messages.user.errors.verificationEmailExpired)
		}

		if (
			user.email &&
			user.email !== user.verifying_email &&
			validator.isEmail(user.email)
		)
			mailer.sendMailTemplate(
				'user/email-changed.pug',
				{ email },
				{
					to: user.email,
					subject: 'Email Updated',
				}
			)

		const { ok, value: updatedUser } = await collection.findOneAndUpdate(
			{ _id: user._id },
			{
				$set: {
					email,
					email_verification: '',
					verifying_email: '',
					email_verified: true,
					email_verification_request_date: null,
					updated_at: Date.now(),
				},
			},
			{ returnDocument: 'after' }
		)

		if (!ok) throw new ApiError(messages.generalError, { report: true })

		return performLogin(updatedUser, { mongo, req, res })
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

module.exports.verifyEmail = verifyEmail

const sendPasswordResetEmail = async (email, { req, mongo, userLoader }) => {
	const collection = await mongo.collection(userCollectionName)

	const alreadyVerifying = await collection.findOne({
		email,
		password_reset_verification: { $ne: null },
	})

	if (alreadyVerifying) {
		const verificationRequestDate = moment(
			alreadyVerifying.password_reset_request_date
		).add(1, 'hour')

		if (moment().isSameOrBefore(verificationRequestDate)) {
			const waitPeriod = moment
				.duration(verificationRequestDate.diff(moment()))
				.humanize()

			throw new ApiError(
				messages.user.errors.passwordResetRequestTooSoon(waitPeriod)
			)
		}
	}

	const passwordResetHash = randomHexValue(20)

	const { ok, value: user } = await collection.findOneAndUpdate(
		{ email },
		{
			$set: {
				password_reset_verification: passwordResetHash,
				password_reset_request_date: Date.now(),
				updated_at: Date.now(),
			},
		},
		{ returnDocument: 'after' }
	)

	if (!user) throw new ApiError(messages.user.errors.resettingPassword)

	if (!ok) throw new ApiError(messages.generalError, { report: true })

	userLoader.prime(user._id, user)

	await mailer.sendMailTemplate(
		'user/password-reset.pug',
		{
			url: `${req.protocol}://${
				process.env.DOMAIN +
				apiUrlConfig.api +
				apiUrlConfig.user +
				apiUrlConfig.passwordReset
			}/${encodeURIComponent(email)}/${encodeURIComponent(passwordResetHash)}`,
		},
		{
			to: email,
			subject: 'Password Reset',
		}
	)
}

module.exports.sendPasswordResetEmail = sendPasswordResetEmail

const verifyPasswordReset = async ({
	email: dirtyEmail,
	hash,
	mongo,
	req,
	res,
}) => {
	try {
		if (!dirtyEmail || !validator.isEmail(dirtyEmail))
			throw new ApiError(messages.user.errors.invalidEmail(dirtyEmail))

		const email = dirtyEmail.toLowerCase()

		const collection = await mongo.collection(userCollectionName)
		const user = await collection.findOne({ email })

		if (!user) throw new ApiError(messages.user.errors.userDoesNotExist)

		if (
			user.password_reset_verification !== hash ||
			!user.password_reset_request_date
		) {
			throw new ApiError(messages.user.errors.invalidInfo)
		}

		if (
			moment(user.password_reset_request_date)
				.add(1, 'days')
				.isSameOrBefore(Date.now())
		) {
			throw new ApiError(messages.user.errors.passwordResetExpired)
		}

		return performLogin(user, { mongo, req, res })
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

module.exports.verifyPasswordReset = verifyPasswordReset

module.exports.isAdmin = (user) => user.roles && user.roles.includes('admin')
/*	user.created_at = new Date(user.created_at)

	if (user.updated_at)
		user.updated_at = new Date(user.updated_at)

	if (user.auth_token_updated)
		user.auth_token_updated = new Date(user.auth_token_updated)

	if (user.email_verification_request_date)
		user.email_verification_request_date = new Date(user.email_verification_request_date)

	if (user.password_reset_request_date)
		user.password_reset_request_date = new Date(user.password_reset_request_date) */
