const User = require('./type')
const userQueries = require('./queries')
const userMutations = require('./mutations')
const userResolver = require('./resolver')
const userInit = require('./init')
const util = require('./util')

module.exports = {
	User,
	userQueries,
	userMutations,
	userResolver,
	userInit,
	...util,
}
