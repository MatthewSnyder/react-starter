const { connectionArguments } = require('../util')

module.exports = `

	getCurrentUser: User
	getUsers(${connectionArguments}): UserConnection
	getUserByAuthToken(authToken: ID!): User!
	getUserById(id: ID!): User!

`
