const { ObjectID } = require('mongodb')
const db = require('../../helpers/db')
const { errorHandler } = require('../util')
const messages = require('../../config/messages')

const { userCollectionName } = require('./vars')

module.exports = async (keys) => {
	try {
		const collection = await db.collection(userCollectionName)

		const objectIDKeys = []

		keys.forEach((key) => objectIDKeys.push(ObjectID(key)))

		const users = await collection
			.find({
				_id: {
					$in: objectIDKeys,
				},
			})
			.toArray()

		return users
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}
