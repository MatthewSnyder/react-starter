const logger = require('../../helpers/logger')

const { userCollectionName } = require('./vars')

module.exports = async (db) => {
	try {
		const users = await db.collection(userCollectionName)
		users.createIndex('email', { unique: true })
	} catch (error) {
		logger.error(error)
		throw error
	}
}
