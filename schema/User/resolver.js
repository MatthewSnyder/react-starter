const bcrypt = require('bcryptjs')
const validator = require('validator')
const axios = require('axios')
const { ObjectID } = require('mongodb')
const { OAuth2Client } = require('google-auth-library')

const logger = require('../../helpers/logger')
const { limitQuery, errorHandler } = require('../util')
const ApiError = require('../../helpers/api-error')
const messages = require('../../config/messages')

const { userCollectionName, requireEmailAuthentication } = require('./vars')

const {
	isAdmin,
	hashPassword,
	userExists,
	sendVerificationEmail,
	verifyPasswordReset,
	sendPasswordResetEmail,
	prepareUser,
	authenticate,
	performLogin,
	socialLogin,
	logout,
} = require('./util')

module.exports = {
	User: {
		id: (user) => user._id || user.id,
		password: () => null,
		created_at: (user) => new Date(user.created_at),
		updated_at: (user) => new Date(user.updated_at),
		auth_token_updated: (user) =>
			user.auth_token_updated ? new Date(user.auth_token_updated) : null,
		email_verification_request_date: (user) =>
			user.email_verification_request_date
				? new Date(user.email_verification_request_date)
				: null,
		password_reset_request_date: (user) =>
			user.password_reset_request_date
				? new Date(user.password_reset_request_date)
				: null,
		has_password: (user) => !!user.password,
	},

	UserEdge: {
		node: (user) => user,
		cursor: (user) => ({ value: user._id || user.id }),
	},

	Query: {
		getCurrentUser(obj, params, { currentUser }) {
			try {
				if (!currentUser) return null
				return currentUser
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async getUsers(
			obj,
			{ limit, before, after, sortBy, sortOrder = -1 },
			{ mongo, currentUser }
		) {
			try {
				if (!currentUser) throw new ApiError(messages.user.errors.notLoggedIn)

				if (!isAdmin(currentUser))
					throw new ApiError(messages.user.errors.permission)

				const collection = await mongo.collection(userCollectionName)

				const { query, pageInfo } = await limitQuery(
					collection,
					{},
					limit,
					sortBy,
					[
						'_id',
						'id',
						'first_name',
						'last_name',
						'username',
						'email',
						'has_password',
						'auth_token_updated',
						'verifying_email',
						'email_verification_request_date',
						'email_verified',
						'password_reset_request_date',
						'created_at',
						'updated_at',
						'roles',
						'settings',
					],
					sortOrder,
					before,
					after
				)

				let edges = await query.toArray()

				if (edges.length) {
					pageInfo.endCursor = { value: edges[edges.length - 1]._id }
					edges = edges.map(prepareUser)
				}

				return {
					edges,
					pageInfo,
				}
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async getUserByAuthToken(obj, { authToken }, { userLoader, mongo }) {
			try {
				const collection = await mongo.collection(userCollectionName)
				const user = await collection.findOne({ auth_token: authToken })

				if (user) {
					userLoader.prime(user._id, user)
					return prepareUser(user)
				}

				throw new ApiError(messages.user.errors.invalidLogin)
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async getUserById(obj, { id }, { userLoader, currentUser }) {
			try {
				if (!currentUser) throw new ApiError(messages.user.errors.notLoggedIn)

				if (!isAdmin(currentUser))
					throw new ApiError(messages.user.errors.permission)

				const user = await userLoader.load(id)
				return prepareUser(user)
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},
	},

	Mutation: {
		async register(obj, { email: dirtyEmail, password }, context) {
			try {
				const email = dirtyEmail.toLowerCase()

				if (!validator.isEmail(email))
					throw new ApiError(messages.user.errors.invalidEmail(email))

				const collection = await context.mongo.collection(userCollectionName)
				const existingUser = await collection.findOne({ email })

				if (existingUser) throw new ApiError(messages.user.errors.userExists)

				const user = {
					email,
					password: hashPassword(password),
					created_at: Date.now(),
					updated_at: Date.now(),
				}

				const { insertedId } = await collection.insertOne(user)

				if (!insertedId)
					throw new ApiError(messages.generalError, { report: true })

				user._id = insertedId
				context.userLoader.prime(user._id, user)

				logger.info({
					message: 'New user created',
					user,
				})

				if (requireEmailAuthentication) {
					try {
						await sendVerificationEmail(email, context)
						return { message: messages.user.confirmations.confirmEmail }
					} catch (error) {
						await collection.findOneAndDelete({ email })
						throw error
					}
				} else
					return {
						...(await performLogin(user, context)),
						message: messages.user.confirmations.login,
					}
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async login(obj, { email: dirtyEmail, password }, context) {
			try {
				const email = dirtyEmail.toLowerCase()
				const user = await authenticate(email, password, context)
				const { user: loggedInUser, jwt } = await performLogin(user, context)

				return {
					jwt,
					user: loggedInUser,
					message: messages.user.confirmations.login,
				}
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async facebookLogin(
			obj,
			{ accessToken, id: facebookId, email: dirtyEmail },
			context
		) {
			try {
				const email = dirtyEmail.toLowerCase()

				if (!validator.isEmail(email))
					throw new ApiError(messages.user.errors.invalidEmail(email))

				const response = await axios.get(
					`https://graph.facebook.com/${process.env.FB_APP_VERSION}/debug_token?input_token=${accessToken}&access_token=${process.env.FB_APP_ID}|${process.env.FB_APP_SECRET}`
				)

				if (
					response.data &&
					response.data.data &&
					response.data.data.is_valid === true &&
					response.data.data.app_id === process.env.FB_APP_ID &&
					response.data.data.user_id === facebookId
				) {
					const { jwt, user } = await socialLogin(
						email,
						{ facebook_id: facebookId },
						context
					)

					return { jwt, user, message: messages.user.confirmations.login }
				}

				throw new ApiError(messages.user.errors.socialVerification)
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async googleLogin(obj, { email, idToken, expiresAt }, context) {
			try {
				if (!validator.isEmail(email))
					throw new ApiError(messages.user.errors.invalidEmail(email))

				if (expiresAt <= Date.now())
					throw new ApiError(messages.user.errors.socialVerification)

				const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, '', '')

				return new Promise((resolve) => {
					client.verifyIdToken(
						{
							idToken,
							audience: process.env.GOOGLE_CLIENT_ID,
						},
						(error, login) => {
							if (error) throw error

							const payload = login.getPayload()

							if (
								payload.aud !== process.env.GOOGLE_CLIENT_ID ||
								!validator.isEmail(payload.email) ||
								payload.email !== email
							) {
								throw new ApiError(messages.user.errors.socialVerification)
							}

							const userID = payload.sub

							return socialLogin(email, { google_id: userID }, context).then(
								({ jwt, user }) =>
									resolve({
										jwt,
										user,
										message: messages.user.confirmations.login,
									})
							)
						}
					)
				}).catch((error) => errorHandler(error, messages.generalError))
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async logout(obj, args, { req, res }) {
			try {
				logout(req, res)
				return messages.user.confirmations.logout
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async updateEmail(obj, { email: dirtyEmail, password }, context) {
			try {
				const { currentUser } = context

				if (!currentUser) throw new ApiError(messages.user.errors.notLoggedIn)

				const oldEmail = currentUser.email.toLowerCase()
				const newEmail = dirtyEmail.toLowerCase()

				if (!validator.isEmail(oldEmail))
					throw new ApiError(messages.user.errors.invalidEmail(oldEmail))

				if (!validator.isEmail(newEmail))
					throw new ApiError(messages.user.errors.invalidEmail(newEmail))

				if (!userExists({ email: oldEmail }, context))
					throw new ApiError(messages.user.errors.userDoesNotExist)

				const collection = await context.mongo.collection(userCollectionName)
				const user = await collection.findOne({ email: oldEmail })

				if (!user)
					throw new ApiError(messages.user.errors.emailNotFound(oldEmail))

				if (!bcrypt.compareSync(password, user.password))
					throw new ApiError(messages.user.errors.invalidPassword)

				await sendVerificationEmail(oldEmail, context, newEmail)
				return messages.user.confirmations.confirmEmail
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async addPassword(obj, { email: dirtyEmail, password }, context) {
			try {
				const { mongo, currentUser } = context

				const email = dirtyEmail.toLowerCase()

				if (!validator.isEmail(dirtyEmail))
					return new ApiError(messages.user.errors.invalidEmail(dirtyEmail))

				if (!currentUser) throw new ApiError(messages.user.errors.notLoggedIn)

				if (!userExists({ email }, context))
					throw new ApiError(messages.user.errors.userDoesNotExist)

				const collection = await mongo.collection(userCollectionName)

				if (!currentUser || currentUser.password)
					throw new ApiError(messages.user.errors.invalidInfo)

				const { ok, value: updatedUser } = await collection.findOneAndUpdate(
					{ email },
					{
						$set: {
							password: hashPassword(password),
							updated_at: Date.now(),
						},
					},
					{ returnDocument: 'after' }
				)

				if (!ok) throw new ApiError(messages.generalError, { report: true })

				await performLogin(updatedUser, context)

				return messages.user.confirmations.addedPassword
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async updatePassword(
			obj,
			{ email: dirtyEmail, currentPassword, newPassword },
			context
		) {
			try {
				const { currentUser } = context

				const email = dirtyEmail.toLowerCase()

				if (!validator.isEmail(dirtyEmail))
					return new ApiError(messages.user.errors.invalidEmail(dirtyEmail))

				if (!currentUser) throw new ApiError(messages.user.errors.notLoggedIn)

				if (!userExists({ email }, context))
					throw new ApiError(messages.user.errors.userDoesNotExist)

				const { mongo, userLoader } = context

				if (!(await authenticate(email, currentPassword, context)))
					throw new ApiError(messages.user.errors.invalidPassword)

				if (currentPassword === newPassword)
					throw new ApiError(messages.user.errors.samePassword)

				const collection = await mongo.collection(userCollectionName)

				const { ok, value: updatedUser } = await collection.findOneAndUpdate(
					{ email },
					{
						$set: {
							password: hashPassword(newPassword),
							updated_at: Date.now(),
						},
					},
					{ returnDocument: 'after' }
				)

				if (!ok) throw new ApiError(messages.user.errors.updatingPassword)

				userLoader.prime(updatedUser._id, updatedUser)

				await performLogin(updatedUser, context)

				return messages.user.confirmations.updatedPassword
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async passwordResetRequest(obj, { email: dirtyEmail }, context) {
			try {
				const email = dirtyEmail.toLowerCase()

				if (!validator.isEmail(dirtyEmail))
					return new ApiError(messages.user.errors.invalidEmail(dirtyEmail))

				if (!userExists({ email }, context))
					throw new ApiError(messages.user.errors.userDoesNotExist)

				await sendPasswordResetEmail(email, context)

				return messages.user.confirmations.passwordResetEmail
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async passwordReset(obj, { email: dirtyEmail, password, hash }, context) {
			try {
				const { currentUser, req, mongo, userLoader } = context

				if (!currentUser) throw new ApiError(messages.user.errors.notLoggedIn)

				if (!dirtyEmail || !validator.isEmail(dirtyEmail))
					throw new ApiError(messages.user.errors.invalidEmail(dirtyEmail))

				const email = dirtyEmail.toLowerCase()

				// only reason we even allow an email to be passed is for future admin password reset functionality
				if (currentUser.email !== email)
					throw new ApiError(
						"You are not the same user as the one who's password is being reset."
					)

				if (!userExists({ email }, context))
					throw new ApiError(messages.user.errors.userDoesNotExist)

				const { user } = await verifyPasswordReset({ email, hash, mongo, req })
				const collection = await mongo.collection(userCollectionName)

				const { ok, value: updatedUser } = await collection.findOneAndUpdate(
					{ email: user.email },
					{
						$set: {
							password: hashPassword(password),
							password_reset_verification: '',
							password_reset_request_date: null,
							updated_at: Date.now(),
						},
					},
					{ returnDocument: 'after' }
				)

				if (!ok) throw new ApiError(messages.generalError, { report: true })

				userLoader.prime(updatedUser._id, updatedUser)

				await performLogin(updatedUser, context)

				return messages.user.confirmations.passwordReset
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},

		async deleteUserById(obj, { id }, { currentUser, mongo, userLoader }) {
			try {
				if (!currentUser) throw new ApiError(messages.user.errors.notLoggedIn)

				if (!isAdmin(currentUser))
					throw new ApiError(messages.user.errors.permission)

				const collection = await mongo.collection(userCollectionName)

				const { deletedCount } = await collection.deleteOne({
					_id: new ObjectID(id),
				})

				if (deletedCount !== 1)
					throw new ApiError(messages.generalError, { report: true })

				userLoader.clear(id)
				return deletedCount
			} catch (error) {
				throw errorHandler(error, messages.generalError)
			}
		},
	},
}
