module.exports = `

	login(email: String!, password: String!): LogInPayload!
	logout: String!
	register(email: String!, password: String!): RegistrationPayload!
	facebookLogin(accessToken: String!, id: ID!, email: String!): LogInPayload!
	googleLogin(email: String!, idToken: String!, expiresAt: GraphQLLong!): LogInPayload!
	updateEmail(email: String!, password: String!): String!
	addPassword(email: String!, password: String!): String!
	updatePassword(email: String!, currentPassword: String!, newPassword: String!): String!
	passwordResetRequest(email: String!): String!
	passwordReset(email: String!, password: String!, hash: String!): String!
	deleteUserById(id: ID!): Boolean!

`
