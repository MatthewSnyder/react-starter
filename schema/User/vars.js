module.exports.authTokenLength = 128
module.exports.userCollectionName = 'users'
module.exports.requireEmailAuthentication =
	process.env.REQUIRE_EMAIL_AUTHENTICATION !== 'false'
