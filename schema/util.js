const { ObjectID } = require('mongodb')
const mailer = require('../helpers/mailer')
const ApiError = require('../helpers/api-error')
const messages = require('../config/messages')

module.exports.prepareNode = (object) => {
	return {
		...object,
		id: object._id.toString(),
	}
}

module.exports.connectionArguments =
	'sortBy: String, sortOrder: SortOrder, limit: Int, before: Cursor, after: Cursor'

const errorHandler = (error, userMessage) => {
	if (error.name === 'ApiError') return error

	mailer.reportError(error)

	return new ApiError(userMessage)
}

module.exports.errorHandler = errorHandler

module.exports.applyPagination = async (query, first, last) => {
	try {
		let count

		if (first || last) {
			count = await query.clone().count()

			let limit
			let skip

			if (first && count > first) limit = first

			if (last) {
				if (limit && limit > last) {
					skip = limit - last
					limit -= skip
				} else if (!limit && count > last) skip = count - last
			}

			if (skip) query.skip(skip)

			if (limit) query.limit(limit)
		}

		return {
			hasNextPage: Boolean(first && count > first),
			hasPreviousPage: Boolean(last && count > last),
		}
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}

/* module.exports.cursorPagination = (query, sortBy = '_id', sortOrder = -1, before, after) => {
	if (sortBy === 'id')
		sortBy = '_id'

	return new Promise((resolve, reject) => {
		return co(function*() {
			let previousQuery = before ? query.clone() : undefined,
					nextQuery = undefined,
					hasPrevious = false,
					hasNext = false

			if (sortBy !== '_id') {
				query.sort([[sortBy, sortOrder], ['_id', sortOrder]])
				
				if (previousQuery)
					previousQuery.sort([[sortBy, sortOrder], ['_id', sortOrder]])
			} else {
				query.sort({ '_id': sortOrder })
				
				if (previousQuery)
					previousQuery.sort({ '_id': sortOrder === -1 ? 1 : -1 })
			}

			nextQuery = query.clone()

			if (query.cmd.limit !== 0)
				hasNext = yield nextQuery.limit(query.cmd.limit + 1).count() > query.cmd.limit

			resolve({ hasPrevious, hasNext })
		}).catch(error => errorHandler(error, messages.generalError, reject))
	})
} */

module.exports.limitQueryWithId = (query, before, after) => {
	if (!before && !after) return query

	const filter = { _id: {} }

	if (before) filter._id.$lt = new ObjectID(before)

	if (after) filter._id.$gt = new ObjectID(after)

	return query.filter(filter)
}

module.exports.limitQuery = async (
	collection,
	queryOptions = {},
	limit = 10,
	sortBy = '_id',
	sortByAcceptableOptions,
	sortOrder = -1,
	before,
	after
) => {
	try {
		let sortByFixed

		if (!Array.isArray(sortByAcceptableOptions))
			throw new ApiError(messages.generalError, { report: true })

		if (!sortByAcceptableOptions.includes(sortBy))
			throw new ApiError(messages.schema.errors.invalidInput)

		/* eslint-disable security/detect-object-injection */

		if (sortBy === 'id') sortByFixed = '_id'
		else sortByFixed = sortBy

		let hasPreviousPage = false
		let hasNextPage = false

		let filter = { ...queryOptions }
		let previousFilter = { ...queryOptions }
		let nextFilter = { ...queryOptions }

		const limits = {}
		const previousLimits = {}
		const nextLimits = {}

		const ors = []
		const previousOrs = []
		const nextOrs = []

		if (after) {
			const op = sortOrder === 1 ? '$gt' : '$lt'
			const reverseOp = sortOrder === 1 ? '$lt' : '$gt'

			if (sortByFixed && sortByFixed !== '_id') {
				const afterObject = await collection.findOne(
					{
						_id: new ObjectID(after),
					},
					{
						fields: { [sortByFixed]: 1 },
					}
				)

				limits[op] = afterObject[sortByFixed]
				previousLimits[reverseOp] = afterObject[sortByFixed]
				nextLimits[op] = afterObject[sortByFixed]

				ors.push({
					[sortByFixed]: afterObject[sortByFixed],
					_id: { [op]: new ObjectID(after) },
				})

				previousOrs.push({
					[sortByFixed]: afterObject[sortByFixed],
					_id: { [reverseOp]: new ObjectID(after) },
				})

				nextOrs.push({
					[sortByFixed]: afterObject[sortByFixed],
					_id: { [op]: new ObjectID(after) },
				})
			} else {
				ors.push({ _id: { [op]: new ObjectID(after) } })

				const cursor = collection
					.find(
						{
							_id: { [op]: new ObjectID(after) },
						},
						{
							_id: 1,
						}
					)
					.sort({
						_id: sortOrder,
					})
					.limit(limit)

				const afterObjects = await cursor.toArray()

				if (afterObjects.length) {
					previousOrs.push({
						_id: { [reverseOp]: new ObjectID(afterObjects[0]._id) },
					})
					nextOrs.push({
						_id: {
							[op]: new ObjectID(afterObjects[afterObjects.length - 1]._id),
						},
					})
				} else nextOrs.push({ _id: { [op]: new ObjectID(after) } })
			}
		}

		if (before) {
			const op = sortOrder === 1 ? '$lt' : '$gt'
			const reverseOp = sortOrder === 1 ? '$gt' : '$lt'

			if (sortByFixed && sortByFixed !== '_id') {
				const beforeObject = await collection.findOne(
					{
						_id: new ObjectID(before),
					},
					{
						fields: { [sortByFixed]: 1 },
					}
				)

				// have to limit how far back we go otherwise it'll
				// just get the ones at the end of the query instead
				// of the ones directly before the queried before item
				if (!after) {
					const cursor = collection
						.find(
							{
								_id: { [op]: new ObjectID(before) },
								[sortByFixed]: { [op]: beforeObject[sortByFixed] },
							},
							{
								_id: 1,
							}
						)
						.sort({
							[sortByFixed]: sortOrder,
							_id: sortOrder,
						})

					const count = await cursor.count()

					if (count > limit) {
						const beforeObjects = await cursor.skip(count - 1 - limit).toArray()

						limits[reverseOp] = beforeObjects[0]._id
					}
				}

				limits[op] = beforeObject[sortByFixed]
				nextLimits[reverseOp] = beforeObject[sortByFixed]
				previousLimits[op] = beforeObject[sortByFixed]

				ors.push({
					[sortByFixed]: beforeObject[sortByFixed],
					_id: { [op]: new ObjectID(before) },
				})

				nextOrs.push({
					[sortByFixed]: beforeObject[sortByFixed],
					_id: { [reverseOp]: new ObjectID(before) },
				})

				previousOrs.push({
					[sortByFixed]: beforeObject[sortByFixed],
					_id: { [op]: new ObjectID(before) },
				})
			} else {
				const cursor = collection
					.find(
						{
							_id: { [op]: new ObjectID(before) },
						},
						{
							_id: 1,
						}
					)
					.sort({
						_id: sortOrder,
					})

				const count = await cursor.count()

				if (!count || after || count <= limit)
					ors.push({ _id: { [op]: new ObjectID(before) } })

				if (count) {
					if (count > limit) cursor.skip(count - 1 - limit)

					const beforeObjects = await cursor.toArray()

					nextOrs.push({
						_id: {
							[reverseOp]: new ObjectID(
								beforeObjects[beforeObjects.length - 1]._id
							),
						},
					})

					if (!after && count > limit) {
						// have to limit how far back we go otherwise it'll
						// just get the ones at the end of the query instead
						// of the ones directly before the queried before item
						ors.push({
							_id: {
								[op]: new ObjectID(before),
								[reverseOp]: new ObjectID(beforeObjects[0]._id),
							},
						})
					} else if (after)
						previousOrs.push({ _id: { [op]: new ObjectID(after) } })
					else
						previousOrs.push({
							_id: { [op]: new ObjectID(beforeObjects[0]._id) },
						})
				} else previousOrs.push({ _id: { [op]: new ObjectID(before) } })
			}
		}

		if (before || after) {
			if (sortByFixed && sortByFixed !== '_id') {
				filter.$or = [{ [sortByFixed]: limits }, ...ors]

				previousFilter.$or = [{ [sortByFixed]: previousLimits }, ...previousOrs]

				nextFilter.$or = [{ [sortByFixed]: nextLimits }, ...nextOrs]
			} else {
				if (ors.length && ors.length > 1) filter.$or = ors
				else if (ors.length) filter = Object.assign(filter, ors[0])

				if (previousOrs.length && previousOrs.length > 1)
					previousFilter.$or = previousOrs
				else if (previousOrs.length)
					previousFilter = Object.assign(previousFilter, previousOrs[0])

				if (nextOrs.length && nextOrs.length > 1) nextFilter.$or = nextOrs
				else if (nextOrs.length)
					nextFilter = Object.assign(nextFilter, nextOrs[0])
			}
		}

		const query = collection.find(filter)
		const previousQuery = collection.find(previousFilter)
		const nextQuery = collection.find(nextFilter)

		if (sortByFixed) {
			query.sort([
				[sortByFixed, sortOrder],
				['_id', sortOrder],
			])
			previousQuery.sort([
				[sortByFixed, sortOrder],
				['_id', sortOrder],
			])
			nextQuery.sort([
				[sortByFixed, sortOrder],
				['_id', sortOrder],
			])
		} else {
			query.sort({ _id: sortOrder })
			previousQuery.sort({ _id: sortOrder === -1 ? 1 : -1 })
			nextQuery.sort({ _id: sortOrder })
		}

		if (limit) query.limit(limit)

		if (after || (before && previousOrs))
			hasPreviousPage = !!(await previousQuery.count())

		if (before) hasNextPage = !!(await nextQuery.count())
		else if (limit) hasNextPage = !!(await nextQuery.skip(limit).count(true))

		// console.log(require('util').inspect(query.cmd), require('util').inspect(previousQuery.cmd), require('util').inspect(nextQuery.cmd), hasPreviousPage, hasNextPage)

		return {
			query,
			pageInfo: { hasPreviousPage, hasNextPage },
		}

		/* eslint-enable security/detect-object-injection */
	} catch (error) {
		throw errorHandler(error, messages.generalError)
	}
}
