const logger = require('../helpers/logger')

const { userInit } = require('./User')

module.exports = async (db) => {
	try {
		await userInit(db)
	} catch (error) {
		logger.error(error)
		throw error
	}
}
