const Base64URL = require('base64-url')
const { GraphQLScalarType } = require('graphql')
const { Kind } = require('graphql/language')

function toCursor({ value }) {
	return Base64URL.encode(value.toString())
}

function fromCursor(string) {
	const value = Base64URL.decode(string)

	if (value) return value
	return null
}

module.exports = new GraphQLScalarType({
	name: 'Cursor',

	serialize(value) {
		if (value) return toCursor(value)
		return null
	},

	parseLiteral(ast) {
		if (ast.kind === Kind.STRING) return fromCursor(ast.value)
		return null
	},

	parseValue(value) {
		return fromCursor(value)
	},
})
