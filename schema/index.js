/* eslint-disable no-underscore-dangle */
const _ = require('lodash')
const {
	DateTimeResolver,
	DateResolver,
	TimeResolver,
} = require('graphql-scalars')
const GraphQLLong = require('./scalars/GraphQLLong')
const Cursor = require('./scalars/Cursor')
const { User, userQueries, userMutations, userResolver } = require('./User')

const typeDefs = `

	schema {
		query: Query
		mutation: Mutation
	}

	scalar Date
	scalar Time
	scalar DateTime

	scalar GraphQLLong

	scalar Cursor

	type Query {
		${userQueries}
	}

	type Mutation {
		${userMutations}
	}

	interface Node {
		id: ID!
	}

	interface Connection {
		edges: [Edge!]
		pageInfo: PageInfo
	}

	interface Edge {
		node: Node!
		cursor: Cursor!
	}

	type PageInfo {
		endCursor: Cursor
		hasNextPage: Boolean!
		hasPreviousPage: Boolean!
	}

	enum SortOrder {
		ASC
		DEC
	}

`

const rootResolver = _.merge(userResolver)

module.exports = {
	typeDefs: [typeDefs, User],
	resolvers: {
		Node: {
			__resolveType(obj) {
				return obj.__typename
			},
			id: (root) => root._id || root.id,
		},
		Edge: {
			__resolveType(obj) {
				return obj.__typename
			},
		},
		Connection: {
			__resolveType(obj) {
				return obj.__typename
			},
		},
		SortOrder: {
			ASC: 1,
			DEC: -1,
		},
		...rootResolver,
		DateTime: DateTimeResolver,
		Date: DateResolver,
		Time: TimeResolver,
		GraphQLLong,
		Cursor,
	},
}
