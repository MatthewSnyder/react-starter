import React from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import { loadableReady } from '@loadable/component'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { onError } from 'apollo-link-error'
import { ApolloLink } from 'apollo-link'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HelmetProvider } from 'react-helmet-async'

import { isElementAtAllInViewportTop, isElementInViewport } from 'root/util'

import Root from './Root'
import configureStore from './store'

const initialState = window.__REDUX_STATE__

const reduxState = document.getElementById('redux-state')

if (reduxState) reduxState.remove()

const apolloClient = new ApolloClient({
	link: ApolloLink.from([
		onError(({ graphQLErrors, networkError }) => {
			if (graphQLErrors)
				graphQLErrors.map(({ message, locations, path }) =>
					console.error(
						`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
					)
				)

			if (networkError) console.error(`[Network error]: ${networkError}`)
		}),
		new HttpLink({
			uri: `${__API_HOST__}/graphql`,
			credentials: 'include',
		}),
	]),
	cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
})

const apolloState = document.getElementById('apollo-state')

if (apolloState) apolloState.remove()

const jss = document.getElementById('jss-server-side')

const store = configureStore(apolloClient, initialState)

const mountNode = document.getElementById('app')

loadableReady(() => {
	hydrate(
		<HelmetProvider>
			<Router>
				<Root store={store} apollo={apolloClient} />
			</Router>
		</HelmetProvider>,
		mountNode
	)

	if (jss) jss.remove()

	if (module.hot)
		module.hot.accept('./Root', () => {
			const NewRoot = require('./Root').default

			hydrate(
				<HelmetProvider>
					<Router>
						<NewRoot store={store} apollo={apolloClient} />
					</Router>
				</HelmetProvider>,
				mountNode
			)
		})
})

let scrollAnimationsCall
let performingScrollAnimations

function scrollAnimations() {
	document
		.querySelectorAll(
			'.fade-in-scroll:not(.show), .fade-in-left-scroll:not(.show), .fade-in-right-scroll:not(.show)'
		)
		.forEach((fadeIn) => {
			if (isElementAtAllInViewportTop(fadeIn)) fadeIn.classList.add('show')
			return null
		})

	document
		.querySelectorAll('.header:not(.animated), .copy:not(.animated)')
		.forEach((animation) => {
			if (isElementInViewport(animation)) animation.classList.add('animated')
			return null
		})

	performingScrollAnimations = false
}

window.scrollAnimationsRequest = () => {
	if (!performingScrollAnimations) {
		performingScrollAnimations = true
		scrollAnimationsCall()
	}
}

if (typeof window.requestAnimationFrame === 'function')
	scrollAnimationsCall = () => {
		window.requestAnimationFrame(scrollAnimations)
	}
else scrollAnimationsCall = scrollAnimations

window.addEventListener('scroll', window.scrollAnimationsRequest, {
	passive: true,
})

window.onload = window.scrollAnimationsRequest
