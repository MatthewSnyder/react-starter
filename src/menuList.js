let menuKey = 0

function makeMenuLink(menuLink) {
	menuKey += 1

	return Object.assign(menuLink, {
		id: menuKey,
		to: typeof menuLink.to === 'object' ? menuLink.to.path : menuLink.to,
	})
}

const homeLink = makeMenuLink({
	to: '/',
	activeOnlyWhenExact: true,
	text: 'Home',
	preload: 'Home',
})

const aboutLink = makeMenuLink({
	to: '/about',
	text: 'About',
	preload: 'About',
})

const contactLink = makeMenuLink({
	to: '/contact',
	text: 'Contact',
	preload: 'Contact',
})

const registerLink = makeMenuLink({
	to: '/register',
	text: 'Register',
	preload: 'Register',
})

const accountLink = makeMenuLink({
	to: '/account',
	text: 'Account',
	preload: 'Account',
})

const adminLink = makeMenuLink({
	to: '/admin',
	text: 'Admin',
	preload: 'Admin',
})

const notFoundLink = makeMenuLink({
	to: '/404',
	text: '404',
	preload: 'NotFound',
})

export default function generateMenuList(isLoggedIn, isAdmin) {
	const menuList = [homeLink, aboutLink, contactLink]

	if (!isLoggedIn) menuList.push(registerLink)

	if (isLoggedIn) menuList.push(accountLink)

	if (isAdmin) menuList.push(adminLink)

	menuList.push(notFoundLink)

	return menuList
}
