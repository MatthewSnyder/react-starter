import React from 'react'
import path from 'path'
import util from 'util'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'
import { ChunkExtractor } from '@loadable/server'
import { ApolloClient } from 'apollo-client'
import { getDataFromTree } from '@apollo/react-ssr'
import { SchemaLink } from 'apollo-link-schema'
import { onError } from 'apollo-link-error'
import { ApolloLink } from 'apollo-link'
// import { RedisCache } from 'apollo-server-cache-redis' seems complicated? https://github.com/apollographql/apollo-server/tree/master/packages/apollo-server-cache-redis
import { InMemoryCache } from 'apollo-cache-inmemory'
import { makeExecutableSchema } from '@graphql-tools/schema'
import { HelmetProvider } from 'react-helmet-async'
import { ServerStyleSheets } from '@material-ui/styles'

import { displayNotification } from 'containers/Notifications/actions'
import DataLoader from 'dataloader'
import indexTemplate from './index.pug'
import errorTemplate from './error.pug'
import configureStore from './store'
import messages from './messages'

const logger = require('../helpers/logger')

const schema = makeExecutableSchema(require('../schema'))

const mongo = require('../helpers/db')

const userLoader = require('../schema/User/loader')

const getUserLoader = () =>
	new DataLoader(userLoader, { cacheKeyFn: (key) => key.toString() })

let Root = require('./Root').default

const serverRenderer = (webpackstats) => (req, res) => {
	let usedSchema = schema

	// Possibly allows schema to be auto updated on each request for dev mode
	if (__IS_DEV__) usedSchema = makeExecutableSchema(require('../schema'))

	const apolloCache = new InMemoryCache()

	/* if (__REDIS_APOLLO_HOST__ && __REDIS_APOLLO_PORT__ && __REDIS_APOLLO_DB__)
		apolloCache = new RedisCache({
			host: __REDIS_APOLLO_HOST__,
			port: __REDIS_APOLLO_PORT__,
			db: __REDIS_APOLLO_DB__
		}) */

	const apolloClient = new ApolloClient({
		link: ApolloLink.from([
			onError(({ graphQLErrors, networkError }) => {
				if (graphQLErrors)
					graphQLErrors.map(({ message, locations, path: errorPath }) =>
						logger.error(
							`[GraphQL error]: Message: ${message}, Location: ${util.inspect(
								locations,
								{ depth: 2 }
							)}, Path: ${errorPath}`
						)
					)

				if (networkError) logger.error(`[Network error]: ${networkError}`)
			}),
			new SchemaLink({
				schema: usedSchema,
				context: {
					req,
					res,
					mongo,
					currentUser: req.session ? req.session.user : false,
					userLoader: getUserLoader(),
				},
			}),
		]),
		cache: apolloCache,
		ssrMode: true,
	})

	const store = configureStore(apolloClient, {})

	if (req.session) {
		const { confirmations, information, errors } = req.session

		if (confirmations.length)
			confirmations.forEach((confirmation) =>
				store.dispatch(
					displayNotification({ message: confirmation, success: true })
				)
			)

		if (information.length)
			information.forEach((info) =>
				store.dispatch(
					displayNotification({ message: info, information: true })
				)
			)

		if (errors.length)
			errors.forEach((error) =>
				store.dispatch(displayNotification({ message: error, error: true }))
			)

		req.session.confirmations = []
		req.session.information = []
		req.session.errors = []
	}

	try {
		const statsFile = path.resolve(
			`.${webpackstats.clientStats.publicPath}loadable-stats.json`
		)
		const chunkExtractor = new ChunkExtractor({ statsFile })
		const sheets = new ServerStyleSheets()

		getDataFromTree(
			<HelmetProvider context={{}}>
				<StaticRouter location={req.url} context={{}}>
					<Root
						store={store}
						apollo={apolloClient}
						userAgent={req.headers['user-agent'] || 'all'}
					/>
				</StaticRouter>
			</HelmetProvider>
		)
			.then(() => {
				const helmetContext = {}
				const routerContext = {}

				const markup = renderToString(
					chunkExtractor.collectChunks(
						sheets.collect(
							<HelmetProvider context={helmetContext}>
								<StaticRouter location={req.url} context={routerContext}>
									<Root
										store={store}
										apollo={apolloClient}
										userAgent={req.headers['user-agent'] || 'all'}
									/>
								</StaticRouter>
							</HelmetProvider>
						)
					)
				)

				if (routerContext.url) {
					res.status(301)
					res.redirect(routerContext.url)
				} else {
					const { helmet } = helmetContext

					const indexHTML = indexTemplate({
						isProd: __IS_PROD__,
						isDev: __IS_DEV__,
						__FB_APP_ID__,
						__GOOGLE_CLIENT_ID__,
						head: `${
							helmet.title.toString() +
							helmet.meta.toString() +
							helmet.link.toString() +
							chunkExtractor.getStyleTags()
						}<style id="jss-server-side">${sheets.toString()}</style>`,
						reduxState: `window.__REDUX_STATE__ = ${JSON.stringify(
							store.getState()
						)}`,
						apolloState: `window.__APOLLO_STATE__ = ${JSON.stringify(
							apolloClient.extract()
						)}`,
						app: markup,
						scripts: chunkExtractor.getScriptTags(),
					})

					let { status } = routerContext

					if (!status) status = 200

					res.status(status)
					res.set('content-type', 'text/html')
					res.send(indexHTML)
				}
			})
			.catch((error) => {
				throw error
			})
	} catch (error) {
		logger.error(error)
		res.status(500)
		res.send(
			errorTemplate({
				error: `${messages.generalError}<br/><br/>${error}`,
			})
		)
	}
}

export default serverRenderer

if (module.hot)
	module.hot.accept('./Root', () => {
		Root = require('./Root')
	})
