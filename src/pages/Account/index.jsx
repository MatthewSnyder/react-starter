import React from 'react'
import { useQuery } from '@apollo/react-hooks'
import { useDispatch } from 'react-redux'
import { Helmet } from 'react-helmet-async'
import md5 from 'md5'

import { CURRENT_USER } from 'containers/User'

import AjaxProgress from 'components/AjaxProgress'
import EmailUpdateForm from 'containers/EmailUpdateForm'
import PasswordUpdateForm from 'containers/PasswordUpdateForm'
import PasswordAddForm from 'containers/PasswordAddForm'

import { apolloErrorHandler } from 'root/util'
import ErrorPage from 'pages/ErrorPage'

import styles from './account.module.less'

export default function Account() {
	const dispatch = useDispatch()

	const { loading, data, error } = useQuery(CURRENT_USER, {
		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	if (loading) return <AjaxProgress />

	if (error) return <ErrorPage error={error} />

	const user = data.getCurrentUser

	if (!user) return <ErrorPage />

	return (
		<div className="page">
			<Helmet>
				<title>Account</title>
			</Helmet>

			<h1>Account</h1>

			<div id={styles['account-information']}>
				<a
					href={`https://www.gravatar.com/${user.email}`}
					target="_blank"
					rel="noopener noreferrer"
				>
					{!!user.email && (
						<img
							id={styles['profile-photo']}
							src={`https://www.gravatar.com/avatar/${md5(user.email)}`}
							alt="Profile"
						/>
					)}
				</a>
				<h2 id={styles.email}>{user.email}</h2>
			</div>

			<div id={styles['account-forms']}>
				<EmailUpdateForm />
				{user.has_password && <PasswordUpdateForm />}
				{!user.has_password && <PasswordAddForm />}
			</div>
		</div>
	)
}
