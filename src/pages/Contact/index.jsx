import React from 'react'
import { Helmet } from 'react-helmet-async'

const Contact = () => (
	<div className="page">
		<Helmet>
			<title>Contact</title>
		</Helmet>

		<h1>Contact</h1>
	</div>
)

export default Contact
