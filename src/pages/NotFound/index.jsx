import React from 'react'
import { Helmet } from 'react-helmet-async'

import styles from './not-found.module.less'

const NotFound = () => (
	<div id={styles['not-found']} className="page">
		<Helmet>
			<title>404</title>
		</Helmet>
		<h1>404</h1>
	</div>
)

export default NotFound
