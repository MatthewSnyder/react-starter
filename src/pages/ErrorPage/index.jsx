import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet-async'

import messages from 'root/messages'

function ErrorPage({ error }) {
	let errorMessage = error

	if (typeof error === 'object') errorMessage = error.message

	return (
		<div className="page">
			<Helmet>
				<title>Error</title>
			</Helmet>

			<h1>Error</h1>

			<h5 style={{ padding: '0 3rem' }}>{errorMessage}</h5>
		</div>
	)
}

ErrorPage.propTypes = {
	error: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
}

ErrorPage.defaultProps = {
	error: messages.react.generalError,
}

export default ErrorPage
