import React from 'react'
import { Helmet } from 'react-helmet-async'
import PasswordResetRequestForm from 'containers/PasswordResetRequestForm'

import styles from './password-reset-request-page.module.less'

const PasswordResetRequestPage = () => (
	<div id={styles.page} className="page">
		<Helmet>
			<title>Password Reset Request</title>
		</Helmet>

		<PasswordResetRequestForm />
	</div>
)

export default PasswordResetRequestPage
