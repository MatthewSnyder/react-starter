import React from 'react'
import { Helmet } from 'react-helmet-async'

const About = () => (
	<div className="page">
		<Helmet>
			<title>About</title>
		</Helmet>
		<h1>About</h1>
	</div>
)

export default About
