import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { useDispatch } from 'react-redux'
import { Helmet } from 'react-helmet-async'
import md5 from 'md5'
import Button from '@material-ui/core/Button'

import AjaxProgress from 'components/AjaxProgress'

import { userQL } from 'containers/User'

import ErrorPage from 'pages/ErrorPage'

import { apolloErrorHandler } from 'root/util'

import styles from './admin.module.less'

const GET_USERS = gql`
	query GetUsers($sortBy: String, $sortOrder: SortOrder, $limit: Int, $before: Cursor, $after: Cursor) {
		getUsers(sortBy: $sortBy, sortOrder: $sortOrder, limit: $limit, before: $before, after: $after) {
			edges {
				node {
					${userQL}
				}

				cursor
			}

			pageInfo {
				endCursor
				hasNextPage
				hasPreviousPage
			}
		}
	}
`

export default function Admin() {
	const dispatch = useDispatch()

	const { loading, error, data, fetchMore } = useQuery(GET_USERS)

	if (loading) return <AjaxProgress />

	if (error || (!loading && (!data || !data.getUsers))) {
		if (error) apolloErrorHandler(error, dispatch)
		return <ErrorPage error={error} />
	}

	const { edges: users, pageInfo } = data.getUsers

	let userCounter = 0

	return (
		<div className="page">
			<Helmet>
				<title>Admin</title>
			</Helmet>
			<h1>Admin</h1>

			<div id={styles.users}>
				<div id={styles.articles}>
					{users.map((_user) => {
						const user = _user.node
						userCounter = (userCounter + 1) % 2

						return (
							<div
								key={user.id}
								className={`${styles.user} ${
									userCounter % 2 === 0 ? styles.even : styles.odd
								}`}
							>
								<a
									href={`https://www.gravatar.com/${user.email}`}
									target="_blank"
									rel="noopener noreferrer"
								>
									{!!user.email && (
										<img
											id={styles['profile-photo']}
											src={`https://www.gravatar.com/avatar/${md5(user.email)}`}
											alt="Profile"
										/>
									)}
								</a>
								<h2 id={styles.email}>{user.email}</h2>
							</div>
						)
					})}

					{pageInfo && (pageInfo.hasNextPage || pageInfo.hasPreviousPage) && (
						<div id={styles.pager}>
							{pageInfo.hasPreviousPage && (
								<div id={styles['pager-prev']}>
									<Button
										color="primary"
										onClick={() =>
											fetchMore({
												variables: { before: users[0].cursor },
												updateQuery: (previousResult, { fetchMoreResult }) => {
													const newEdges = fetchMoreResult.getUsers.edges
													const { newPageInfo } = fetchMoreResult.getUsers

													return newEdges.length
														? {
																// Put the new users at the end of the list and update `pageInfo`
																// so we have the new `endCursor` and `hasNextPage` values
																getUsers: {
																	__typename:
																		previousResult.getUsers.__typename,
																	edges: newEdges,
																	pageInfo: newPageInfo,
																},
														  }
														: previousResult
												},
											})
										}
										variant="contained"
									>
										Prev
									</Button>
								</div>
							)}

							{pageInfo.hasNextPage && (
								<div id={styles['pager-next']}>
									<Button
										color="primary"
										onClick={() =>
											fetchMore({
												variables: {
													after: users[users.length - 1].cursor,
												},
												updateQuery: (previousResult, { fetchMoreResult }) => {
													const newEdges = fetchMoreResult.getUsers.edges
													const { newPageInfo } = fetchMoreResult.getUsers

													return newEdges.length
														? {
																// Put the new users at the end of the list and update `pageInfo`
																// so we have the new `endCursor` and `hasNextPage` values
																getUsers: {
																	__typename:
																		previousResult.getUsers.__typename,
																	edges: newEdges,
																	pageInfo: newPageInfo,
																},
														  }
														: previousResult
												},
											})
										}
										variant="contained"
									>
										Next
									</Button>
								</div>
							)}
						</div>
					)}
				</div>
			</div>
		</div>
	)
}
