import React from 'react'
import { useParams } from 'react-router'
import PasswordResetForm from 'containers/PasswordResetForm'
import { Helmet } from 'react-helmet-async'

import styles from './password-reset-page.module.less'

export default function PasswordResetPage() {
	const params = useParams()

	const { email } = params
	const { hash } = params

	return (
		<div id={styles.page} className="page">
			<Helmet>
				<title>Reset Password</title>
			</Helmet>

			<PasswordResetForm email={email} hash={hash} />
		</div>
	)
}
