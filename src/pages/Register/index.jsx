import React from 'react'
import { Helmet } from 'react-helmet-async'

import Registration from 'containers/Registration'

import styles from './register.module.less'

const Register = () => (
	<div id={styles.register} className="page">
		<Helmet>
			<title>Register</title>
		</Helmet>
		<h1>Register</h1>

		<Registration />
	</div>
)

export default Register
