import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { ApolloProvider } from '@apollo/react-hooks'
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles'
import { blue, red } from '@material-ui/core/colors'
import Layout from 'containers/Layout'

/* eslint-disable-next-line import/no-webpack-loader-syntax */
import styles from '!less-vars-loader!global-styles/vars.less'

function Root({ store, apollo, userAgent }) {
	const muiThemeOptions = {
		palette: {
			primary: { main: styles.primaryColor },
			secondary: blue,
			error: red,
		},

		overrides: {
			MuiInputBase: {
				root: { margin: '0 0 15px' },
				input: { padding: '10px 15px' },
			},

			MuiButton: {
				root: { margin: '0 0 15px' },
			},
		},
	}

	if (userAgent) muiThemeOptions.userAgent = userAgent

	const theme = createTheme(muiThemeOptions)

	return (
		<Provider store={store}>
			<ApolloProvider client={apollo}>
				<MuiThemeProvider theme={theme}>
					<Layout />
				</MuiThemeProvider>
			</ApolloProvider>
		</Provider>
	)
}

Root.propTypes = {
	/* eslint-disable react/forbid-prop-types */
	store: PropTypes.object.isRequired,
	apollo: PropTypes.object.isRequired,
	/* eslint-enable react/forbid-prop-types */
	userAgent: PropTypes.string,
}

Root.defaultProps = {
	userAgent: '',
}

export default Root
