import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import keycode from 'keycode'

import styles from './overlay-wrapper.module.less'

function OverlayWrapper({ id, children, dismissOverlay, ...props }) {
	const wrapper = useRef(null)

	function handleKeyDown(e) {
		if (e.keyCode === keycode('escape')) dismissOverlay()
	}

	useEffect(() => {
		document.addEventListener('keydown', handleKeyDown, false)

		return () => {
			document.removeEventListener('keydown', handleKeyDown)
		}
	})

	let { animate, classNames, show } = props

	if (typeof show === 'undefined') show = !!children

	if (typeof animate === 'undefined') animate = true

	if (classNames && classNames.length) classNames = ` ${classNames}`
	else classNames = ''

	let render = <div />

	if (animate)
		render = (
			<TransitionGroup component={null}>
				{show && (
					<CSSTransition
						timeout={3500}
						classNames={{
							enter: styles.enter,
							enterActive: styles['enter-active'],
							exit: styles.exit,
							exitActive: styles['exit-active'],
						}}
						appear
					>
						<div
							id={id}
							className={styles.wrapper + classNames}
							role="button"
							tabIndex="0"
							ref={wrapper}
							onMouseDown={(e) => {
								if (e.target === wrapper.current) dismissOverlay()
							}}
							onKeyDown={(e) => {
								if (e.target === wrapper.current) dismissOverlay()
							}}
						>
							{children}
						</div>
					</CSSTransition>
				)}
			</TransitionGroup>
		)
	else if (show)
		render = (
			<div
				id={id}
				className={styles.wrapper + classNames}
				role="button"
				tabIndex="0"
				ref={wrapper}
				onMouseDown={(e) => {
					if (e.target === wrapper.current) dismissOverlay()
				}}
				onKeyDown={(e) => {
					if (e.target === wrapper.current) dismissOverlay()
				}}
			>
				{children}
			</div>
		)

	return render
}

OverlayWrapper.propTypes = {
	name: PropTypes.string,
	children: PropTypes.node.isRequired,
	animate: PropTypes.bool,
	show: PropTypes.bool.isRequired,
	dismissOverlay: PropTypes.func.isRequired,
	id: PropTypes.string,
	classNames: PropTypes.string,
}

export default OverlayWrapper
