import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import loadable from '@loadable/component'

import styles from './navigation.module.less'

const Navigation = ({ list }) => (
	<nav id={styles.nav}>
		<ul>
			{list.map((item) => {
				let Page

				if (item.preload) Page = loadable(() => import(`pages/${item.preload}`))

				return (
					<li key={item.id}>
						<NavLink
							to={item.to}
							onMouseOver={() => item.preload && Page.preload()}
							onFocus={() => item.preload && Page.preload()}
							activeClassName={styles.active}
							exact={item.activeOnlyWhenExact}
						>
							<span>{item.text}</span>
						</NavLink>
					</li>
				)
			})}
		</ul>
	</nav>
)

Navigation.propTypes = {
	list: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default Navigation
