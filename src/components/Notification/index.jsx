import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames/bind'

import styles from './notification.module.less'

const cx = classNames.bind(styles)

function Notification({ message, success, error }) {
	const classes = cx({
		notification: true,
		success,
		error,
	})

	return <div className={classes}>{message}</div>
}

Notification.propTypes = {
	message: PropTypes.string.isRequired,
	success: PropTypes.bool,
	error: PropTypes.bool,
}

Notification.defaultProps = {
	success: false,
	error: false,
}

export default Notification
