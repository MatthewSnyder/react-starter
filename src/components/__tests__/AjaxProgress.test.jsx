import React from 'react'
import AjaxProgress from '../AjaxProgress'

describe('AjaxProgress', () => {
	it('should render as expected', () => {
		expect(<AjaxProgress />).toMatchSnapshot()
	})
})
