import React from 'react'
import PropTypes from 'prop-types'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import styles from './route-transitions.module.less'

const RouteTransitions = ({
	transitionKey,
	transitionName,
	appear,
	appearTimeout,
	enterTimeout,
	exitTimeout,
	children,
}) => (
	<TransitionGroup component={null}>
		<CSSTransition
			key={transitionKey}
			classNames={{
				appear: styles[`${transitionName}-appear`],
				appearActive: styles[`${transitionName}-appear-active`],
				appearDone: styles[`${transitionName}-appear-done`],
				enter: styles[`${transitionName}-enter`],
				enterActive: styles[`${transitionName}-enter-active`],
				enterDone: styles[`${transitionName}-enter-done`],
				exit: styles[`${transitionName}-exit`],
				exitActive: styles[`${transitionName}-exit-active`],
				exitDone: styles[`${transitionName}-exit-done`],
			}}
			appear={appear}
			timeout={{
				enter: enterTimeout,
				exit: exitTimeout,
				appear: appearTimeout,
			}}
		>
			{children}
		</CSSTransition>
	</TransitionGroup>
)

RouteTransitions.propTypes = {
	transitionKey: PropTypes.string.isRequired,
	children: PropTypes.node.isRequired,
	transitionName: PropTypes.string,
	appear: PropTypes.bool,
	appearTimeout: PropTypes.number,
	enterTimeout: PropTypes.number,
	exitTimeout: PropTypes.number,
}

RouteTransitions.defaultProps = {
	transitionName: 'slide',
	appear: false,
	appearTimeout: 1000,
	enterTimeout: 500,
	exitTimeout: 500,
}

export default RouteTransitions
