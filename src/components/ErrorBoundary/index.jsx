import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ErrorPage from 'pages/ErrorPage'

import messages from 'root/messages'

export default class ErrorBoundary extends PureComponent {
	constructor(props) {
		super(props)
		this.state = { hasError: false }
	}

	static getDerivedStateFromError() {
		return { hasError: true }
	}

	componentDidCatch(error, errorInfo) {
		console.error(error, errorInfo)
	}

	render() {
		const {
			props: { children, errorMessage },
			state: { hasError },
		} = this

		if (hasError) return <ErrorPage error={errorMessage} />

		return children
	}
}

ErrorBoundary.propTypes = {
	children: PropTypes.node.isRequired,
	errorMessage: PropTypes.string,
}

ErrorBoundary.defaultProps = {
	errorMessage: messages.react.generalError,
}
