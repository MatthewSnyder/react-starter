import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet-async'

const OpenGraph = ({
	title,
	type,
	image,
	url,
	audio,
	description,
	determiner,
	locale,
	localeAlternate,
	siteName,
	video,
}) => (
	<Helmet>
		<meta property="og:title" content={title} />
		<meta property="og:type" content={type} />
		<meta property="og:image" content={image} />
		<meta property="og:url" content={url} />
		{audio && <meta property="og:audio" content={audio} />}
		{description && <meta property="og:description" content={description} />}
		{determiner && <meta property="og:determiner" content={determiner} />}
		{locale && <meta property="og:locale" content={locale} />}
		{localeAlternate && (
			<meta property="og:locale:alternate" content={localeAlternate} />
		)}
		{siteName && <meta property="og:site_name" content={siteName} />}
		{video && <meta property="og:video" content={video} />}
	</Helmet>
)

OpenGraph.propTypes = {
	title: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	image: PropTypes.string.isRequired,
	url: PropTypes.string,
	audio: PropTypes.string,
	description: PropTypes.string,
	determiner: PropTypes.string,
	locale: PropTypes.string,
	localeAlternate: PropTypes.string,
	siteName: PropTypes.string,
	video: PropTypes.string,
}

OpenGraph.defaultProps = {
	url: typeof window === 'object' ? window.location.href : '',
	audio: '',
	description: '',
	determiner: '',
	locale: '',
	localeAlternate: '',
	siteName: '',
	video: '',
}

export default OpenGraph

export function OpenGraphArticle({ author, section, tag, ...props }) {
	let { publishedTime, modifiedTime, expirationTime } = props

	if (typeof publishedTime === 'number')
		publishedTime = new Date(publishedTime).toISOString()
	else if (typeof publishedTime === 'object')
		publishedTime = publishedTime.toISOString()

	if (typeof modifiedTime === 'number')
		modifiedTime = new Date(modifiedTime).toISOString()
	else if (typeof modifiedTime === 'object')
		modifiedTime = modifiedTime.toISOString()

	if (typeof expirationTime === 'number')
		expirationTime = new Date(expirationTime).toISOString()
	else if (typeof expirationTime === 'object')
		expirationTime = expirationTime.toISOString()

	let authors = author

	if (author && !Array.isArray(author)) authors = [author]

	let tags = tag

	if (tag && !Array.isArray(tag)) tags = [tag]

	return (
		<Helmet>
			{publishedTime && (
				<meta property="article:published_time" content={publishedTime} />
			)}
			{modifiedTime && (
				<meta property="article:modified_time" content={modifiedTime} />
			)}
			{expirationTime && (
				<meta property="article:expiration_time" content={expirationTime} />
			)}
			{authors &&
				authors.map((a) => (
					<meta key={a} property="article:author" content={a} />
				))}
			{section && <meta property="article:section" content={section} />}
			{tags &&
				tags.map((t) => (
					<meta key={author} property="article:tag" content={t} />
				))}
		</Helmet>
	)
}

OpenGraphArticle.propTypes = {
	publishedTime: PropTypes.oneOf([
		PropTypes.object,
		PropTypes.string,
		PropTypes.number,
	]),
	modifiedTime: PropTypes.oneOf([
		PropTypes.object,
		PropTypes.string,
		PropTypes.number,
	]),
	expirationTime: PropTypes.oneOf([
		PropTypes.object,
		PropTypes.string,
		PropTypes.number,
	]),
	author: PropTypes.oneOf([PropTypes.string, PropTypes.array]),
	section: PropTypes.string,
	tag: PropTypes.oneOf([PropTypes.string, PropTypes.array]),
}

OpenGraphArticle.defaultProps = {
	publishedTime: '',
	modifiedTime: '',
	expirationTime: '',
	author: '',
	section: '',
	tag: '',
}
