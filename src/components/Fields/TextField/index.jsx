import React from 'react'
import PropTypes from 'prop-types'
import { TextField as TF } from '@material-ui/core'

import styles from '../fields.module.less'

const TextField = ({
	field, // { name, value, onChange, onBlur }
	form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
	type,
	className,
	...custom
}) => {
	const isError = !!(touched[field.name] && errors[field.name])

	return (
		<div className={styles['form-field']}>
			<TF
				type={type}
				className={`${className} ${isError ? styles.error : ''}`}
				error={isError}
				{...field} /* eslint-disable-line react/jsx-props-no-spreading */
				{...custom} /* eslint-disable-line react/jsx-props-no-spreading */
			/>

			{isError && (
				<div className={`${styles['form-messages']} ${styles.error}`}>
					{errors[field.name]}
				</div>
			)}
		</div>
	)
}

/* eslint-disable react/forbid-prop-types */
TextField.propTypes = {
	field: PropTypes.shape({
		name: PropTypes.string.isRequired,
		value: PropTypes.string,
		onChange: PropTypes.func.isRequired,
		onBlur: PropTypes.func.isRequired,
	}).isRequired,
	form: PropTypes.shape({
		touched: PropTypes.object.isRequired,
		errors: PropTypes.object.isRequired,
	}).isRequired,
	type: PropTypes.string,
	className: PropTypes.string,
}
/* eslint-enable react/forbid-prop-types */

TextField.defaultProps = {
	type: 'text',
	className: '',
}

export default TextField
