import React from 'react'
import PropTypes from 'prop-types'

import styles from '../fields.module.less'

const OptionsField = ({
	inputId,
	input,
	label,
	className,
	meta: { touched, dirty, error, warning },
}) => (
	<div className={styles['redux-form-field']}>
		<label htmlFor={inputId}>
			{label}
			<input
				// eslint-disable-next-line react/jsx-props-no-spreading
				{...input}
				placeholder={label}
				className={`${className} ${touched && error ? styles.error : ''} ${
					warning ? styles.warning : ''
				}`}
				type="text"
			/>
		</label>
		{touched &&
			dirty &&
			((error && (
				<div className={`${styles['form-messages']} ${styles.error}`}>
					{error}
				</div>
			)) ||
				(warning && (
					<div className={`${styles['form-messages']} ${styles.warning}`}>
						{warning}
					</div>
				)))}
	</div>
)

/* eslint-disable react/forbid-prop-types */
OptionsField.propTypes = {
	inputId: PropTypes.string.isRequired,
	options: PropTypes.shape({
		value: PropTypes.string,
		required: PropTypes.bool,
		selected: PropTypes.bool,
	}).isRequired,
	multiple: PropTypes.bool,
	list: PropTypes.bool,
	label: PropTypes.string.isRequired,
	className: PropTypes.string,
	input: PropTypes.object.isRequired,
	meta: PropTypes.object.isRequired,
}
/* eslint-enable react/forbid-prop-types */

OptionsField.defaultProps = {
	multiple: false,
	list: false,
	className: '',
}

export default OptionsField
