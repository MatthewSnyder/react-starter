import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

import styles from '../fields.module.less'

function DateField({
	inputId,
	input,
	label,
	className,
	dateFormat,
	meta: { touched, dirty, error, warning },
}) {
	let computedDate = input.value

	if (typeof input.value === 'number' && input.value)
		computedDate = moment(input.value, 'X').format(dateFormat)

	return (
		<div className={styles['redux-form-field']}>
			{input.value && (
				<label htmlFor={inputId} className="animated fadeIn">
					{label}

					<input
						id={inputId}
						// eslint-disable-next-line react/jsx-props-no-spreading
						{...input}
						placeholder={label}
						type="text"
						className={`${className} ${touched && error ? styles.error : ''} ${
							warning ? styles.warning : ''
						}`}
						value={computedDate}
					/>
				</label>
			)}
			<div className={styles.hint}>
				Format example: {moment().format(dateFormat)}
			</div>
			{touched &&
				dirty &&
				((error && (
					<div className={`${styles['form-messages']} ${styles.error}`}>
						{error}
					</div>
				)) ||
					(warning && (
						<div className={`${styles['form-messages']} ${styles.warning}`}>
							{warning}
						</div>
					)))}
		</div>
	)
}

/* eslint-disable react/forbid-prop-types */
DateField.propTypes = {
	inputId: PropTypes.string.isRequired,
	input: PropTypes.object.isRequired,
	label: PropTypes.string.isRequired,
	className: PropTypes.string,
	dateFormat: PropTypes.string,
	meta: PropTypes.object.isRequired,
}
/* eslint-enable react/forbid-prop-types */

DateField.defaultProps = {
	className: '',
	dateFormat: 'MMMM Do, YYYY hh:mm A',
}

export default DateField
