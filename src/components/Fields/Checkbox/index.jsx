import React from 'react'
import PropTypes from 'prop-types'
import { FormControlLabel, Checkbox as CB } from '@material-ui/core'

import styles from '../fields.module.less'

export default function Checkbox({
	field, // { name, value, onChange, onBlur }
	form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
	label,
	className,
	...custom
}) {
	const isError = !!(touched[field.name] && errors[field.name])

	return (
		<div className={styles['form-field']}>
			<FormControlLabel
				control={
					<CB
						checked={field.value}
						className={`${className} ${isError ? styles.error : ''}`}
						{...field} /* eslint-disable-line react/jsx-props-no-spreading */
						{...custom} /* eslint-disable-line react/jsx-props-no-spreading */
					/>
				}
				label={label}
			/>

			{isError && (
				<div className={`${styles['form-messages']} ${styles.error}`}>
					{errors[field.name]}
				</div>
			)}
		</div>
	)
}

/* eslint-disable react/forbid-prop-types */
Checkbox.propTypes = {
	field: PropTypes.shape({
		name: PropTypes.string.isRequired,
		value: PropTypes.bool,
		onChange: PropTypes.func.isRequired,
		onBlur: PropTypes.func.isRequired,
	}).isRequired,
	form: PropTypes.shape({
		touched: PropTypes.object.isRequired,
		errors: PropTypes.object.isRequired,
	}).isRequired,
	label: PropTypes.string.isRequired,
	className: PropTypes.string,
}
/* eslint-enable react/forbid-prop-types */

Checkbox.defaultProps = { className: '' }
