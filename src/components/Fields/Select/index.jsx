import React from 'react'
import PropTypes from 'prop-types'
import {
	FormControl,
	InputLabel,
	Select as MSelect,
	MenuItem,
} from '@material-ui/core'

import styles from '../fields.module.less'

const Select = ({
	label,
	id,
	field, // { name, id, value, onChange, onBlur }
	form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
	className,
	options,
	formControlProperties,
	...custom
}) => {
	const isError = !!(touched[field.name] && errors[field.name])

	return (
		<div className={styles['form-field']}>
			<FormControl
				{...formControlProperties} /* eslint-disable-line react/jsx-props-no-spreading */
			>
				{label && <InputLabel htmlFor={id}>{label}</InputLabel>}
				<MSelect
					inputProps={{ name: field.name }}
					className={`${className} ${isError ? styles.error : ''}`}
					error={isError}
					{...field} /* eslint-disable-line react/jsx-props-no-spreading */
					{...custom} /* eslint-disable-line react/jsx-props-no-spreading */
				>
					{options &&
						options.map((option) => (
							<MenuItem key={option.value} value={option.value}>
								{option.display}
							</MenuItem>
						))}
				</MSelect>
			</FormControl>

			{isError && (
				<div className={`${styles['form-messages']} ${styles.error}`}>
					{errors[field.name]}
				</div>
			)}
		</div>
	)
}

/* eslint-disable react/forbid-prop-types */
Select.propTypes = {
	label: PropTypes.string,
	id: PropTypes.string,
	field: PropTypes.shape({
		name: PropTypes.string.isRequired,
		value: PropTypes.string,
		onChange: PropTypes.func.isRequired,
		onBlur: PropTypes.func.isRequired,
	}).isRequired,
	form: PropTypes.shape({
		touched: PropTypes.object.isRequired,
		errors: PropTypes.object.isRequired,
	}).isRequired,
	className: PropTypes.string,
	options: PropTypes.arrayOf(
		PropTypes.shape({
			value: PropTypes.string.isRequired,
			display: PropTypes.string.isRequired,
		})
	).isRequired,
	formControlProperties: PropTypes.shape({ fullWidth: PropTypes.bool }),
}
/* eslint-enable react/forbid-prop-types */

Select.defaultProps = {
	label: null,
	id: '',
	className: '',
	formControlProperties: {},
}

export default Select
