import React from 'react'
import PropTypes from 'prop-types'
import ring from './images/ring.svg'
import styles from './ajax-progress.module.less'

const AjaxProgress = ({ absolute }) => (
	<div id={styles.wrapper} className={absolute ? styles.absolute : ''}>
		<img src={ring} alt="AJAX Loader" />
	</div>
)

AjaxProgress.propTypes = {
	absolute: PropTypes.bool,
}

AjaxProgress.defaultProps = {
	absolute: false,
}

export default AjaxProgress
