import React from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { useDispatch } from 'react-redux'
import { Formik, Field } from 'formik'
import { Button } from '@material-ui/core'

import { TextField } from 'components/Fields'
import AjaxProgress from 'components/AjaxProgress'
import { apolloErrorHandler } from 'root/util'
import { displayNotification } from 'containers/Notifications'

import { CURRENT_USER, signInPayloadQL } from 'containers/User'

const ADD_PASSWORD = gql`
	mutation AddPassword($email: String!, $password: String!) {
		addPassword(email: $email, password: $password) {
			${signInPayloadQL}
		}
	}
`

export default function PasswordAddForm() {
	const dispatch = useDispatch()

	const { loading: userLoading, data: userData } = useQuery(CURRENT_USER, {
		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	const [addPassword, { loading: addPasswordLoading }] = useMutation(
		ADD_PASSWORD,
		{
			onCompleted: ({ addPassword: message }) => {
				dispatch(
					displayNotification({
						success: true,
						message,
					})
				)
			},

			refetchQueries: [
				{
					query: CURRENT_USER,
				},
			],

			onError: (errors) => apolloErrorHandler(errors, dispatch),
		}
	)

	if (userLoading) return <AjaxProgress />

	const currentUser = userData.getCurrentUser

	const initialValues = {
		password: '',
		passwordConfirmation: '',
	}

	return (
		<div className="form">
			<h2>Add Password</h2>

			<Formik
				initialValues={initialValues}
				validate={(values) => {
					const errors = {}

					if (!values.password) errors.password = 'Required'
					else if (values.password.length < 8)
						errors.password = 'Password must be at least 8 characters'

					if (values.password !== values.passwordConfirmation)
						errors.passwordConfirmation = 'Passwords do not match'

					return errors
				}}
				onSubmit={({ password }, { resetForm }) => {
					addPassword({
						variables: {
							email: currentUser.email,
							password,
						},
					})

					resetForm({ values: initialValues })
				}}
			>
				{({ handleSubmit }) => (
					<form onSubmit={handleSubmit}>
						{addPasswordLoading && <AjaxProgress />}

						<Field name="password">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="Password"
									fullWidth
									required
								/>
							)}
						</Field>

						<Field name="passwordConfirmation">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="Password Confirmation"
									fullWidth
									required
								/>
							)}
						</Field>

						<Button
							type="submit"
							fullWidth
							disabled={addPasswordLoading}
							variant="contained"
							color="primary"
						>
							Add Password
						</Button>
					</form>
				)}
			</Formik>
		</div>
	)
}
