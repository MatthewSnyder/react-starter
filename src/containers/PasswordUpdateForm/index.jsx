import React from 'react'
import { useDispatch } from 'react-redux'
import gql from 'graphql-tag'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Formik, Field } from 'formik'
import { Button } from '@material-ui/core'

import { TextField } from 'components/Fields'
import AjaxProgress from 'components/AjaxProgress'
import { apolloErrorHandler } from 'root/util'
import { displayNotification } from 'containers/Notifications'

import { CURRENT_USER } from 'containers/User'

const PASSWORD_UPDATE = gql`
	mutation UpdatePassword(
		$email: String!
		$currentPassword: String!
		$newPassword: String!
	) {
		updatePassword(
			email: $email
			currentPassword: $currentPassword
			newPassword: $newPassword
		)
	}
`

export default function PasswordUpdateForm() {
	const dispatch = useDispatch()

	const { loading: userLoading, data: userData } = useQuery(CURRENT_USER, {
		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	if (userLoading) return <AjaxProgress />

	let currentUser

	if (userData) currentUser = userData.getCurrentUser

	const [updatePassword, { loading: updatePasswordLoading }] = useMutation(
		PASSWORD_UPDATE,
		{
			onCompleted: ({ updatePassword: message }) => {
				dispatch(
					displayNotification({
						success: true,
						message,
					})
				)
			},

			refetchQueries: [
				{
					query: CURRENT_USER,
				},
			],

			onError: (errors) => apolloErrorHandler(errors, dispatch),
		}
	)

	const initialValues = {
		currentPassword: '',
		newPassword: '',
		passwordConfirmation: '',
	}

	return (
		<div className="form">
			<h2>Update Password</h2>
			<Formik
				initialValues={initialValues}
				validate={(values) => {
					const errors = {}

					if (!values.currentPassword) errors.currentPassword = 'Required'

					if (!values.newPassword) errors.password = 'Required'
					else if (values.newPassword.length < 8)
						errors.newPassword = 'Password must be at least 8 characters'

					if (values.newPassword !== values.passwordConfirmation)
						errors.passwordConfirmation = 'Passwords do not match'

					return errors
				}}
				onSubmit={({ currentPassword, newPassword }, { resetForm }) => {
					updatePassword({
						variables: {
							email: currentUser.email,
							currentPassword,
							newPassword,
						},
					})

					resetForm({ values: initialValues })
				}}
			>
				{({ handleSubmit }) => (
					<form onSubmit={handleSubmit}>
						{updatePasswordLoading && <AjaxProgress />}

						<Field name="currentPassword">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="Current Password"
									fullWidth
									required
								/>
							)}
						</Field>

						<Field name="newPassword">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="New Password"
									fullWidth
									required
								/>
							)}
						</Field>

						<Field name="passwordConfirmation">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="Password Confirmation"
									fullWidth
									required
								/>
							)}
						</Field>

						<Button
							type="submit"
							fullWidth
							disabled={updatePasswordLoading}
							variant="contained"
							color="primary"
						>
							Update Password
						</Button>
					</form>
				)}
			</Formik>
		</div>
	)
}
