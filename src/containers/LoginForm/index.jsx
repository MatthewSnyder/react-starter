import React from 'react'
import PropTypes from 'prop-types'
import gql from 'graphql-tag'
import { useMutation, useApolloClient } from '@apollo/react-hooks'
import { useDispatch } from 'react-redux'
import { Formik, Field } from 'formik'
import { Button } from '@material-ui/core'
import validator from 'validator'

import { TextField } from 'components/Fields'
import { displayDialog, dismissDialog } from 'containers/Dialog'
import PasswordResetRequestForm from 'containers/PasswordResetRequestForm'
import SocialLogin from 'containers/SocialLogin'

import AjaxProgress from 'components/AjaxProgress'
import { apolloErrorHandler } from 'root/util'
import { displayNotification } from 'containers/Notifications'

import { signInPayloadQL, loginStatusChange } from 'containers/User'

import styles from './login-form.module.less'

const LOGIN = gql`
	mutation login($email: String!, $password: String!) {
		login(email: $email, password: $password) {
			${signInPayloadQL}
		}
	}
`

export default function LoginForm({ dialogId }) {
	const dispatch = useDispatch()
	const client = useApolloClient()

	const dismissLogin = () => {
		if (dialogId >= 0) dispatch(dismissDialog(dialogId))
	}

	const [login, { loading }] = useMutation(LOGIN, {
		onCompleted: async ({ login: { message } }) => {
			await loginStatusChange(client)

			dismissLogin()

			dispatch(
				displayNotification({
					success: true,
					message,
				})
			)
		},

		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	function forgotPassword() {
		dismissLogin()

		dispatch(
			displayDialog({
				render: function PasswordResetRequestFormWrapper(id) {
					return <PasswordResetRequestForm dialogId={id} />
				},
			})
		)
	}

	const initialValues = {
		email: '',
		password: '',
	}

	return (
		<div id={styles['form-wrapper']} className="form">
			<Formik
				initialValues={initialValues}
				validate={(values) => {
					const errors = {}

					if (!values.email) errors.email = 'Required'
					else if (!validator.isEmail(values.email))
						errors.email = 'Invalid Email'

					if (!values.password) errors.password = 'Required'

					return errors
				}}
				onSubmit={({ email, password }, { resetForm }) => {
					login({
						variables: {
							email,
							password,
						},
					})

					resetForm({
						values: {
							...initialValues,
							email,
						},
					})
				}}
			>
				{({ handleSubmit }) => (
					<form onSubmit={handleSubmit}>
						{loading && <AjaxProgress />}

						<Field name="email">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="email"
									label="Email"
									fullWidth
									required
								/>
							)}
						</Field>

						<Field name="password">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="Password"
									fullWidth
									required
								/>
							)}
						</Field>

						<Button
							type="submit"
							fullWidth
							disabled={loading}
							variant="contained"
							color="primary"
						>
							Login
						</Button>
					</form>
				)}
			</Formik>

			<Button
				id={styles['forgot-password']}
				onClick={() => forgotPassword()}
				onKeyDown={() => forgotPassword()}
			>
				Forgot Password?
			</Button>

			<SocialLogin dialogId={dialogId} />
		</div>
	)
}

LoginForm.propTypes = {
	dialogId: PropTypes.number,
}

LoginForm.defaultProps = {
	dialogId: -1,
}
