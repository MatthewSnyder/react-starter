import React from 'react'
import PropTypes from 'prop-types'
import gql from 'graphql-tag'
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'

import Navigation from 'components/Navigation'

import LoginForm from 'containers/LoginForm'
import { displayDialog } from 'containers/Dialog'
import { displayNotification } from 'containers/Notifications'

import { isInteractiveKeyPress, apolloErrorHandler } from 'root/util'

import { CURRENT_USER, loginStatusChange } from 'containers/User'

import { socialLogout } from 'containers/SocialLogin'

import styles from './header.module.less'

const LOGOUT = gql`
	mutation {
		logout
	}
`

export default function Header({ menuList }) {
	const dispatch = useDispatch()
	const client = useApolloClient()

	const displayLogin = () => {
		dispatch(
			displayDialog({
				render: function LoginFormWrapper(dialogId) {
					return <LoginForm dialogId={dialogId} />
				},
			})
		)
	}

	const [logout] = useMutation(LOGOUT, {
		onCompleted: async ({ logout: message }) => {
			await loginStatusChange(client)
			socialLogout()

			dispatch(
				displayNotification({
					success: true,
					message,
				})
			)
		},

		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	const { data } = useQuery(CURRENT_USER, {
		fetchPolicy: 'cache-and-network',
		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	let currentUser = false

	if (data) currentUser = data.getCurrentUser

	let userLink = (
		<div
			role="button"
			tabIndex="0"
			id={styles['user-link']}
			onClick={() => displayLogin()}
			onKeyDown={(e) => isInteractiveKeyPress(e) && displayLogin()}
		>
			Login
		</div>
	)

	if (currentUser)
		userLink = (
			<div
				role="button"
				tabIndex="0"
				id={styles['user-link']}
				onClick={() => logout()}
				onKeyDown={(e) => isInteractiveKeyPress(e) && logout()}
			>
				Logout
			</div>
		)

	return (
		<header id={styles.container}>
			<div id={styles.header}>
				<Link to="/">
					<span>Environment: </span>
					<span className={styles.sizzle}>{__NODE_ENV__}</span>
				</Link>
			</div>

			<Navigation list={menuList} />
			{userLink}
		</header>
	)
}

Header.propTypes = {
	menuList: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.number.isRequired,
			to: PropTypes.string.isRquired,
		})
	),
}

Header.defaultProps = {
	menuList: [],
}
