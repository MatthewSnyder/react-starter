import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { useApolloClient } from '@apollo/react-hooks'
import { useDispatch } from 'react-redux'

import { loginStatusChange } from 'containers/User'

import { dismissDialog } from 'containers/Dialog'
import { displayNotification } from 'containers/Notifications'

import FacebookLoginButton from './FacebookLoginButton'
import GoogleLoginButton from './GoogleLoginButton'

import styles from './social-login.module.less'

/* const TWITTER_LOGIN = gql`
mutation TwitterLogin() {
	twitterLogin() {
		${signInPayloadQL}
	}
}` */

// import twitterLoginButtonImg from './images/sign-in-with-twitter-gray.png'

export default function SocialLogin({ dialogId }) {
	const dispatch = useDispatch()
	const client = useApolloClient()

	const socialLogin = useCallback(async ({ message }) => {
		await loginStatusChange(client)

		if (dialogId >= 0) dispatch(dismissDialog(dialogId))

		dispatch(
			displayNotification({
				success: true,
				message,
			})
		)
	}, [])

	let fbLoginButton = ''
	let gLoginButton = ''
	// twitterLoginButton = ''

	if (__FB_APP_ID__)
		fbLoginButton = <FacebookLoginButton socialLogin={socialLogin} />
	if (__GOOGLE_CLIENT_ID__)
		gLoginButton = <GoogleLoginButton socialLogin={socialLogin} />

	/* if (__TWITTER_ID__)
			twitterLoginButton = (
				<a href="/twitter/request-token">
					<img id={styles.twitter} src={twitterLoginButtonImg} alt="Twitter Login" />
				</a>
			) */

	return (
		<div id={styles.wrapper}>
			{fbLoginButton}
			{gLoginButton}
			{/* { twitterLoginButton } */}
		</div>
	)
}

export function socialLogout() {
	if (__GOOGLE_CLIENT_ID__)
		window.gapi.load('auth2', () => {
			gapi.auth2
				.init({
					client_id: __GOOGLE_CLIENT_ID__,
					cookiepolicy: 'single_host_origin',
				})
				.then(() => {
					const auth2 = gapi.auth2.getAuthInstance()

					if (auth2.isSignedIn.get()) auth2.signOut()
				})
		})
}

SocialLogin.propTypes = {
	dialogId: PropTypes.number,
}

SocialLogin.defaultProps = {
	dialogId: -1,
}
