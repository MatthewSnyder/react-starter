import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import gql from 'graphql-tag'
import { useApolloClient } from '@apollo/react-hooks'

import { signInPayloadQL } from 'containers/User'

import { apolloErrorHandler } from 'root/util'
import { displayErrorDialog } from 'containers/Dialog'

const FACEBOOK_LOGIN = gql`
	mutation FacebookLogin($accessToken: String!, $id: ID!, $email: String!) {
		facebookLogin(accessToken: $accessToken, id: $id, email: $email) {
			${signInPayloadQL}
		}
	}
`

export default function FacebookLoginButton({ socialLogin }) {
	const dispatch = useDispatch()
	const client = useApolloClient()

	useEffect(() => {
		if (__IS_CLIENT__) {
			console.log(__FB_APP_ID__, window.FB)
			if (__FB_APP_ID__ && window.FB) {
				FB.init({
					appId: __FB_APP_ID__,
					cookie: true, // enable cookies to allow the server to access the session
					xfbml: true, // parse social plugins on this page
					version: __FB_APP_VERSION__,
				})

				window.fbCheckLoginState = () =>
					FB.getLoginStatus((response) => {
						const auth = response.authResponse

						if (response.status === 'connected') {
							FB.api('/me/permissions', (permissionsResponse) => {
								if (permissionsResponse.error) {
									dispatch(
										displayErrorDialog({
											body: 'There was a problem logging you in. Please try again later',
										})
									)
									return
								}

								const { data: permissions } = permissionsResponse
								let permissionsGranted = true

								for (let i = 0; i < permissions.length; i += 1) {
									// eslint-disable-next-line security/detect-object-injection
									if (permissions[i].status !== 'granted') {
										displayErrorDialog({
											body: 'Email is required for logging in.',
										})

										FB.logout()
										permissionsGranted = false
									}
								}

								if (permissionsGranted)
									FB.api(
										'/me?scope=email',
										{
											local: 'en_US',
											fields: 'name, email',
										},
										(user) => {
											client
												.mutate({
													mutation: FACEBOOK_LOGIN,
													variables: {
														accessToken: auth.accessToken,
														id: auth.userID,
														email: user.email,
													},
												})
												.then(({ data, errors }) => {
													if (errors) {
														apolloErrorHandler(errors, dispatch)
													} else if (data.facebookLogin) {
														socialLogin(data.facebookLogin)
													} else throw new Error('Invalid Response')
												})
												.catch((errors) => apolloErrorHandler(errors, dispatch))
										}
									)
							})
						} else if (response.status === 'not_authorized') {
							// The person is logged into Facebook, but not your app.
						} else {
							// The person is not logged into Facebook, so we're not sure if
							// they are logged into this app or not.
						}
					})

				window.fbLogOut = () => FB.logout()

				window.fbAsyncInit = () => {
					FB.init({
						appId: __FB_APP_ID__,
						cookie: true, // enable cookies to allow the server to access the session
						xfbml: true, // parse social plugins on this page
						version: __FB_APP_VERSION__,
					})
				}
			}
		}
	}, [])

	return (
		<div
			className="fb-login-button"
			data-size="large"
			data-button-type="continue_with"
			data-auto-logout-link="false"
			data-use-continue-as="true"
			data-scope="public_profile, email"
			data-onlogin="fbCheckLoginState()"
		/>
	)
}

FacebookLoginButton.propTypes = {
	socialLogin: PropTypes.func.isRequired,
}
