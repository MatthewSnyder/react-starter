import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import gql from 'graphql-tag'
import { useApolloClient } from '@apollo/react-hooks'

import { displayErrorDialog } from 'containers/Dialog'
import { signInPayloadQL } from 'containers/User'

import { apolloErrorHandler } from 'root/util'

import styles from './social-login.module.less'

const GOOGLE_LOGIN = gql`
	mutation GoogleLogin($email: String!, $idToken: String!, $expiresAt: GraphQLLong!) {
		googleLogin(email: $email, idToken: $idToken, expiresAt: $expiresAt) {
			${signInPayloadQL}
		}
	}
`

export default function GoogleLoginButton({ socialLogin }) {
	const googleButton = useRef(null)

	const dispatch = useDispatch()
	const client = useApolloClient()

	useEffect(() => {
		if (__IS_CLIENT__) {
			if (__GOOGLE_CLIENT_ID__ && googleButton && window.gapi) {
				gapi.load('auth2', () => {
					// Retrieve the singleton for the GoogleAuth library and set up the client.
					gapi.auth2.init({
						client_id: __GOOGLE_CLIENT_ID__,
						cookiepolicy: 'single_host_origin',
					})

					gapi.signin2.render(styles.google, {
						onsuccess: (googleUser) => {
							const profile = googleUser.getBasicProfile()

							const authResponse = googleUser.getAuthResponse()

							client
								.mutate({
									mutation: GOOGLE_LOGIN,
									variables: {
										email: profile.getEmail(),
										idToken: authResponse.id_token,
										expiresAt: authResponse.expires_at,
									},
								})
								.then(({ data, errors }) => {
									if (errors) {
										apolloErrorHandler(errors, dispatch)
									} else if (data.googleLogin) {
										socialLogin(data.googleLogin)
									} else throw new Error('Invalid Response')
								})
								.catch((errors) => apolloErrorHandler(errors, dispatch))
						},

						onfailure: (error) => {
							dispatch(
								displayErrorDialog({
									body: 'There was a problem logging you in. Please try again later',
									options: [{ text: 'Ok' }],
								})
							)
							console.log(error)
						},
					})
				})
			}
		}
	}, [])

	return <div id={styles.google} ref={googleButton} data-theme="light" />
}

GoogleLoginButton.propTypes = {
	socialLogin: PropTypes.func.isRequired,
}
