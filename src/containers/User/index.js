import gql from 'graphql-tag'

export const isAdmin = (user) => {
	if (!user) return false
	return user.roles && user.roles.includes('admin')
}

export const loginStatusChange = (client) => {
	return client.clearStore().then(client.resetStore)
}

export const DATE_FORMAT = 'MMMM Do, YYYY hh:mm A'

export const userQL = `
	id
	email
	auth_token
	created_at
	updated_at
	roles
	has_password
	settings {
		subscribed
	}`

export const signInPayloadQL = `
	jwt
	user {
		${userQL}
	}
	message
`

export const CURRENT_USER = gql`
	query {
		getCurrentUser {
			${userQL}
		}
  }`
