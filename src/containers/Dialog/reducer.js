import { handleActions } from 'redux-actions'
import * as actions from './actions'

export const isDialog = (state) => {
	if (!state.dialog.size) return false
	return !!state.dialog.find((dialog) => dialog.get('open') !== false)
}

export default handleActions(
	{
		[actions.displayDialog]: (state, { payload }) => {
			const dialog = payload
			dialog.key = state.keyIncrementer

			return {
				dialog: [...state.dialog, dialog],
				keyIncrementer: state.keyIncrementer + 1,
			}
		},

		[actions.displayConfirmationDialog]: (state, { payload }) => {
			const dialog = payload
			dialog.confirmation = true

			dialog.key = state.keyIncrementer

			return {
				dialog: [...state.dialog, dialog],
				keyIncrementer: state.keyIncrementer + 1,
			}
		},

		[actions.displayInformationDialog]: (state, { payload }) => {
			const dialog = payload
			dialog.information = true

			dialog.key = state.keyIncrementer

			return {
				dialog: [...state.dialog, dialog],
				keyIncrementer: state.keyIncrementer + 1,
			}
		},

		[actions.displayErrorDialog]: (state, { payload }) => {
			const dialog = payload
			dialog.error = true

			dialog.key = state.keyIncrementer

			return {
				dialog: [...state.dialog, dialog],
				keyIncrementer: state.keyIncrementer + 1,
			}
		},

		[actions.initDismissDialog]: (state, { payload }) => ({
			...state,
			dialog: state.dialog.map((dialog) => {
				if (dialog.key === payload)
					return {
						...dialog,
						open: false,
					}
				return dialog
			}),
		}),

		[actions.removeDialog]: (state, { payload }) => ({
			dialog: state.dialog.filter((dialog) => dialog.key !== payload),
			keyIncrementer: state.keyIncrementer - 1,
		}),
	},
	{
		dialog: [],
		keyIncrementer: 0,
	}
)
