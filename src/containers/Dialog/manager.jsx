import React from 'react'
import { useSelector } from 'react-redux'

import Dialog from '.'

export default function DialogManager() {
	const dialogs = useSelector((state) => state.dialog.dialog)

	return (
		<>
			{dialogs.map((dialog) => (
				<Dialog id={dialog.key} key={dialog.key.toString()} dialog={dialog} />
			))}
		</>
	)
}
