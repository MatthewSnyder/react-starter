import { createAction } from 'redux-actions'

export const displayDialog = createAction('DISPLAY_DIALOG')

export const displayConfirmationDialog = createAction(
	'DISPLAY_CONFIRMATION_DIALOG'
)

export const displayInformationDialog = createAction(
	'DISPLAY_INFORMATION_DIALOG'
)

export const displayErrorDialog = createAction('DISPLAY_ERROR_DIALOG')

export const initDismissDialog = createAction('INIT_DISMISS_DIALOG')

export const removeDialog = createAction('REMOVE_DIALOG')

export const dismissDialog = (id) => (dispatch) => {
	dispatch(initDismissDialog(id))

	setTimeout(() => {
		dispatch(removeDialog(id))
	}, 500)
}
