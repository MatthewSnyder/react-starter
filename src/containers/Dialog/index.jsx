import React, { useEffect, useRef } from 'react'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import {
	Dialog as MDialog,
	DialogTitle,
	DialogContent,
	DialogContentText,
	DialogActions,
	Button,
} from '@material-ui/core'

import { dismissDialog as _dismissDialog } from './actions'

import styles from './dialog.module.less'

export default function Dialog({ id, dialog }) {
	const firstButton = useRef(null)

	useEffect(() => {
		if (firstButton.current) firstButton.current.focus()
	})

	const {
		open,
		body,
		render,
		canDismiss,
		confirmation,
		information,
		error,
		options,
	} = dialog

	let { title } = dialog

	const dispatch = useDispatch()

	if (!title) {
		if (confirmation) title = 'Confirmation'
		else if (information) title = 'Information'
		else if (error) title = 'Error'
	}

	const dismissable = typeof canDismiss !== 'undefined' ? canDismiss : true

	const dismissDialog = () => dispatch(_dismissDialog(id))

	const onClose = (event, reason) => {
		if (!dismissable && reason === 'backdropClick') return
		dismissDialog()
	}

	const isOpen = typeof open !== 'undefined' ? open : true

	return (
		<MDialog
			open={isOpen}
			onClose={onClose}
			disableEscapeKeyDown={!dismissable}
		>
			{title && <DialogTitle id={styles.title}>{title}</DialogTitle>}
			{body && (
				<DialogContent>
					<DialogContentText id={styles.body}>{body}</DialogContentText>
				</DialogContent>
			)}
			{render && <DialogContent>{render(id)}</DialogContent>}
			{options && (
				<DialogActions>
					{options.map((item, index) => (
						<Button
							key={item.text}
							autoFocus={index === 0 ? firstButton : null}
							onClick={() => {
								if (typeof item.onClick === 'function') item.onClick()

								if (item.hideOnClick !== false) dismissDialog()
							}}
							onKeyDown={() => {
								if (typeof item.onClick === 'function') item.onClick()

								if (item.hideOnClick !== false) dismissDialog()
							}}
						>
							{item.text}
						</Button>
					))}
				</DialogActions>
			)}
		</MDialog>
	)
}

Dialog.propTypes = {
	id: PropTypes.number.isRequired,
	dialog: PropTypes.shape({
		title: PropTypes.string,
		body: PropTypes.string,
		render: PropTypes.func,
		options: PropTypes.arrayOf(
			PropTypes.shape({
				text: PropTypes.string,
				onClick: PropTypes.func,
				hideOnClick: PropTypes.func,
			})
		),
		confirmation: PropTypes.bool,
		information: PropTypes.bool,
		error: PropTypes.bool,
		open: PropTypes.bool,
		canDismiss: PropTypes.bool,
	}).isRequired,
}

export * from './actions'
export { isDialog } from './reducer'
