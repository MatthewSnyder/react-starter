import React from 'react'
import { useDispatch } from 'react-redux'
import gql from 'graphql-tag'
import { useMutation, useApolloClient } from '@apollo/react-hooks'
import { Formik, Field } from 'formik'
import { Button } from '@material-ui/core'
import validator from 'validator'

import { TextField } from 'components/Fields'
import SocialLogin from 'containers/SocialLogin'
import AjaxProgress from 'components/AjaxProgress'
import { apolloErrorHandler } from 'root/util'
import { displayNotification } from 'containers/Notifications'

import { userQL, loginStatusChange } from 'containers/User'

import styles from './registration.module.less'

const REGISTER = gql`
	mutation Register($email: String!, $password: String!) {
		register(email: $email, password: $password) {
			message
			jwt
			user {
				${userQL}
			}
		}
	}
`

export default function Registration() {
	const dispatch = useDispatch()
	const client = useApolloClient()

	const [register, { loading }] = useMutation(REGISTER, {
		onCompleted: async ({ register: registerPayload }) => {
			if (registerPayload.user && registerPayload.jwt) {
				await loginStatusChange(client)
			}

			dispatch(
				displayNotification({
					success: true,
					message: registerPayload.message,
				})
			)
		},

		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	const initialValues = {
		email: '',
		password: '',
		passwordConfirmation: '',
	}

	return (
		<div className="form">
			<Formik
				initialValues={initialValues}
				validate={(values) => {
					const errors = {}

					if (!values.email) errors.email = 'Required'
					else if (!validator.isEmail(values.email))
						errors.email = 'Invalid Email'

					if (!values.password) errors.password = 'Required'
					else if (values.password.length < 8)
						errors.password = 'Password must be at least 8 characters'

					if (values.password !== values.passwordConfirmation)
						errors.passwordConfirmation = 'Passwords do not match'

					return errors
				}}
				onSubmit={({ email, password }, { resetForm }) => {
					register({
						variables: {
							email,
							password,
						},
					})

					resetForm({ values: initialValues })
				}}
			>
				{({ handleSubmit }) => (
					<form id={styles.registration} onSubmit={handleSubmit}>
						{loading && <AjaxProgress />}

						<Field name="email">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="email"
									label="Email"
									fullWidth
									required
								/>
							)}
						</Field>

						<Field name="password">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="Password"
									fullWidth
									required
								/>
							)}
						</Field>

						<Field name="passwordConfirmation">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="Password Confirmation"
									fullWidth
									required
								/>
							)}
						</Field>

						<Button
							type="submit"
							fullWidth
							disabled={loading}
							variant="contained"
							color="primary"
						>
							Register
						</Button>
					</form>
				)}
			</Formik>

			<SocialLogin />
		</div>
	)
}
