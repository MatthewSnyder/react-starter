import { createAction } from 'redux-actions'

export const addNotification = createAction('ADD_NOTIFICATION')
export const dismissNotification = createAction('DISMISS_NOTIFICATION')

export const displayNotification = (notification) => (dispatch) => {
	dispatch(addNotification(notification))

	setTimeout(() => dispatch(dismissNotification()), 5000)
}

export const dismissServerNotifications = (notifications) => (dispatch) => {
	notifications.forEach((notification, i) => {
		setTimeout(() => dispatch(dismissNotification()), 5000 * i + 5000)
	})
}
