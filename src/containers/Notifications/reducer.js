import { handleActions } from 'redux-actions'
import * as actions from './actions'

export const getNotifications = (state) => state.notifications.notifications

export default handleActions(
	{
		[actions.addNotification]: (state, action) => ({
			notifications: [
				...state.notifications,
				{
					...action.payload,
					key: state.keyIncrementer.toString(),
				},
			],
			keyIncrementer: state.keyIncrementer + 1,
		}),

		[actions.dismissNotification]: (state) => {
			return {
				notifications: state.notifications.filter(
					(notification) => notification.key === state.keyIncrementer
				),
				keyIncrementer: state.keyIncrementer - 1,
			}
		},
	},
	{
		notifications: [],
		keyIncrementer: 0,
	}
)
