import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import Notification from 'components/Notification'

import { dismissServerNotifications } from './actions'
import { getNotifications } from './reducer'

import styles from './notifications.module.less'

function Notifications() {
	const notifications = useSelector(getNotifications)

	const dispatch = useDispatch()

	useEffect(() => {
		if (notifications.length)
			dispatch(dismissServerNotifications(notifications))
	}, [])

	return (
		<div id={styles.notifications}>
			<TransitionGroup component={null}>
				{notifications.map((notification) => (
					<CSSTransition
						key={notification.key}
						timeout={350}
						classNames={{
							enter: styles.enter,
							enterActive: styles['enter-active'],
							exit: styles.exit,
							exitActive: styles['exit-active'],
						}}
					>
						<Notification
							key={notification.key}
							message={notification.message}
							success={notification.success}
							error={notification.error}
						/>
					</CSSTransition>
				))}
			</TransitionGroup>
		</div>
	)
}

export * from './actions'

export default Notifications
