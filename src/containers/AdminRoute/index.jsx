import React from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router'
import { Route, Redirect } from 'react-router-dom'
import { useQuery } from '@apollo/react-hooks'
import { useDispatch } from 'react-redux'

import { CURRENT_USER, isAdmin } from 'containers/User'

import { apolloErrorHandler } from 'root/util'

const Workaround = ({ action, children }) =>
	action === 'REPLACE' ? null : children

export default function AdminRoute({ component: Component, ...rest }) {
	const dispatch = useDispatch()
	const history = useHistory()

	const { data } = useQuery(CURRENT_USER, {
		fetchPolicy: 'cache-and-network',
		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	let currentUser

	if (data) currentUser = data.getCurrentUser

	return (
		<Route
			// eslint-disable-next-line react/jsx-props-no-spreading
			{...rest}
			render={({ location }) =>
				isAdmin(currentUser) ? (
					<Component />
				) : (
					<Workaround action={history.action}>
						<Redirect to={{ pathname: '/', state: { from: location } }} />
					</Workaround>
				)
			}
		/>
	)
}

AdminRoute.propTypes = {
	component: PropTypes.func.isRequired,
}
