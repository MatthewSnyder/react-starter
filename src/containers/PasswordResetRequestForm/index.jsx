import React from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'
import { Formik, Field } from 'formik'
import { Button } from '@material-ui/core'
import validator from 'validator'

import { TextField } from 'components/Fields'
import AjaxProgress from 'components/AjaxProgress'

import { apolloErrorHandler } from 'root/util'
import { dismissDialog } from 'containers/Dialog'
import { displayNotification } from 'containers/Notifications'

const PASSWORD_RESET_REQUEST = gql`
	mutation PasswordResetRequest($email: String!) {
		passwordResetRequest(email: $email)
	}
`

export default function PasswordResetRequestForm({ dialogId }) {
	const dispatch = useDispatch()

	const [passwordResetRequest, { loading }] = useMutation(
		PASSWORD_RESET_REQUEST,
		{
			onCompleted: ({ passwordResetRequest: message }) => {
				if (dialogId >= 0) dispatch(dismissDialog(dialogId))

				dispatch(
					displayNotification({
						success: true,
						message,
					})
				)
			},

			onError: (errors) => apolloErrorHandler(errors, dispatch),
		}
	)

	const initialValues = {
		email: '',
	}

	return (
		<div className="form">
			<Formik
				initialValues={initialValues}
				validate={(values) => {
					const errors = {}

					if (!values.email) errors.email = 'Required'
					else if (!validator.isEmail(values.email))
						errors.email = 'Invalid Email'

					return errors
				}}
				onSubmit={({ email }, { resetForm }) => {
					passwordResetRequest({
						variables: { email },
					})

					resetForm({ values: initialValues })
				}}
			>
				{({ handleSubmit }) => (
					<form onSubmit={handleSubmit}>
						{loading && <AjaxProgress />}

						<Field name="email">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="email"
									label="Email"
									fullWidth
									required
								/>
							)}
						</Field>

						<Button
							type="submit"
							fullWidth
							disabled={loading}
							variant="contained"
							color="primary"
						>
							Reset Password
						</Button>
					</form>
				)}
			</Formik>
		</div>
	)
}

PasswordResetRequestForm.propTypes = {
	dialogId: PropTypes.number,
}

PasswordResetRequestForm.defaultProps = {
	dialogId: -1,
}
