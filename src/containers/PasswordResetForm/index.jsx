import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'
import { Formik, Field } from 'formik'
import { Button } from '@material-ui/core'

import { TextField } from 'components/Fields'
import AjaxProgress from 'components/AjaxProgress'
import { apolloErrorHandler } from 'root/util'
import { displayNotification } from 'containers/Notifications'

import { CURRENT_USER, signInPayloadQL } from 'containers/User'

const PASSWORD_RESET = gql`
	mutation PasswordReset($email: String!, $password: String!, $hash: String!) {
		passwordReset(email: $email, password: $password, hash: $hash) {
			${signInPayloadQL}
		}
	}
`

export default function PasswordResetForm({ email, hash }) {
	const [shouldRedirect, setShouldRedirect] = useState(false)

	const dispatch = useDispatch()

	const [passwordReset, { loading }] = useMutation(PASSWORD_RESET, {
		onCompleted: ({ passwordReset: message }) => {
			dispatch(
				displayNotification({
					success: true,
					message,
				})
			)

			setShouldRedirect(true)
		},

		refetchQueries: [
			{
				query: CURRENT_USER,
			},
		],

		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	if (shouldRedirect) return <Redirect to="/" />

	const initialValues = {
		password: '',
		passwordConfirmation: '',
	}

	return (
		<Formik
			initialValues={initialValues}
			validate={(values) => {
				const errors = {}

				if (!values.password) errors.password = 'Required'
				else if (values.password.length < 8)
					errors.password = 'Password must be at least 8 characters'

				if (values.password !== values.passwordConfirmation)
					errors.passwordConfirmation = 'Passwords do not match'

				return errors
			}}
			onSubmit={({ password }, { resetForm }) => {
				passwordReset({
					variables: {
						email,
						password,
						hash,
					},
				})

				resetForm({ values: initialValues })
			}}
		>
			{({ handleSubmit }) => (
				<form className="form" onSubmit={handleSubmit}>
					{loading && <AjaxProgress />}

					<Field name="password">
						{({ field, form }) => (
							<TextField
								field={field}
								form={form}
								type="password"
								label="Password"
								fullWidth
								required
							/>
						)}
					</Field>

					<Field name="passwordConfirmation">
						{({ field, form }) => (
							<TextField
								field={field}
								form={form}
								type="password"
								label="Password Confirmation"
								fullWidth
								required
							/>
						)}
					</Field>

					<Button
						type="submit"
						fullWidth
						disabled={loading}
						variant="contained"
						color="primary"
					>
						Reset Password
					</Button>
				</form>
			)}
		</Formik>
	)
}

PasswordResetForm.propTypes = {
	email: PropTypes.string.isRequired,
	hash: PropTypes.string.isRequired,
}
