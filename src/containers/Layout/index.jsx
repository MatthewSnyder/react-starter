import React from 'react'
import { useLocation } from 'react-router'
import { Switch, Route } from 'react-router-dom'
import { useQuery } from '@apollo/react-hooks'
import { useSelector, useDispatch } from 'react-redux'
import loadable from '@loadable/component'
import { Helmet } from 'react-helmet-async'

import ErrorBoundary from 'components/ErrorBoundary'
import OpenGraph from 'components/OpenGraph'
import Header from 'containers/Header'
import generateMenuList from 'root/menuList'
import Notifications from 'containers/Notifications'
import AjaxProgress from 'components/AjaxProgress'
import DialogManager from 'containers/Dialog/manager'
import RouteTransitions from 'components/RouteTransitions'
import AnonymousRoute from 'containers/AnonymousRoute'
import UserRoute from 'containers/UserRoute'
import AdminRoute from 'containers/AdminRoute'

import apiURLConfig from 'root/api-url.conf'

import { CURRENT_USER, isAdmin } from 'containers/User'

import { isDialog as _isDialog } from 'containers/Dialog'

import { apolloErrorHandler } from 'root/util'

import styles from './layout.module.less'

require('global-styles/styles.less')

const AsyncPage = loadable((props) => import(`pages/${props.page}`), {
	fallback: (
		<div id="page-loader">
			<AjaxProgress absolute />
		</div>
	),
})

export default function Layout() {
	const location = useLocation()

	const isDialog = useSelector(_isDialog)

	const dispatch = useDispatch()

	const { data } = useQuery(CURRENT_USER, {
		fetchPolicy: 'cache-and-network',
		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	let currentUser

	if (data) currentUser = data.getCurrentUser

	const isCurrentUserAdmin = isAdmin(currentUser)

	const menuList = generateMenuList(!!currentUser, isCurrentUserAdmin)

	return (
		<div className={styles.container}>
			<Helmet defaultTitle="React Starter" titleTemplate="%s | React Starter">
				<title />
				<meta name="description" content="Site Description" />
				<meta name="theme-color" content="#FFFFFF" />
			</Helmet>

			<OpenGraph
				siteName="React Starter"
				title="React Starter"
				description="Site Description"
				type="website"
				url="https://www.example.com"
				image="https://www.example.com/preview.jpg"
			/>

			<div id={styles['page-wrapper']} className={isDialog ? styles.blur : ''}>
				<Header user={!!currentUser} menuList={menuList} />
				<div id={styles['body-wrapper']}>
					<ErrorBoundary errorMessage="Failed to load the page. Please refresh to try again.">
						<RouteTransitions
							transitionKey={location.key ? location.key : ''}
							appear
						>
							<Switch location={location}>
								<Route
									path="/"
									exact
									component={() => <AsyncPage page="Home" />}
								/>
								<Route
									path="/about"
									component={() => <AsyncPage page="About" />}
								/>
								<UserRoute
									path="/account"
									component={() => <AsyncPage page="Account" />}
								/>
								<AdminRoute
									path="/admin"
									component={() => <AsyncPage page="Admin" />}
								/>
								<Route
									path="/contact"
									component={() => <AsyncPage page="Contact" />}
								/>
								<AnonymousRoute
									path="/password-reset"
									component={() => <AsyncPage page="PasswordResetRequest" />}
								/>
								<UserRoute
									path={`${apiURLConfig.passwordReset}/:email/:hash`}
									component={() => <AsyncPage page="PasswordReset" />}
								/>
								<AnonymousRoute
									path="/register"
									component={() => <AsyncPage page="Register" />}
								/>
								<Route
									render={({ staticContext }) => {
										if (staticContext) staticContext.status = 404
										return <AsyncPage page="NotFound" />
									}}
								/>
							</Switch>
						</RouteTransitions>
					</ErrorBoundary>
				</div>
			</div>

			<Notifications />

			<DialogManager />
		</div>
	)
}
