import React from 'react'
import { useDispatch } from 'react-redux'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'
import { Formik, Field } from 'formik'
import { Button } from '@material-ui/core'
import validator from 'validator'

import { TextField } from 'components/Fields'
import AjaxProgress from 'components/AjaxProgress'
import { apolloErrorHandler } from 'root/util'
import { displayNotification } from 'containers/Notifications'

import { CURRENT_USER } from 'containers/User'

const UPDATE_EMAIL = gql`
	mutation UpdateEmail($email: String!, $password: String!) {
		updateEmail(email: $email, password: $password)
	}
`

export default function EmailUpdateForm() {
	const dispatch = useDispatch()

	const [updateEmail, { loading }] = useMutation(UPDATE_EMAIL, {
		onCompleted: ({ updateEmail: message }) => {
			dispatch(
				displayNotification({
					success: true,
					message,
				})
			)
		},

		refetchQueries: [
			{
				query: CURRENT_USER,
			},
		],

		onError: (errors) => apolloErrorHandler(errors, dispatch),
	})

	const initialValues = {
		email: '',
		password: '',
	}

	return (
		<div className="form">
			<h2>Update Email</h2>
			<Formik
				initialValues={initialValues}
				validate={(values) => {
					const errors = {}

					if (!values.email) errors.email = 'Required'
					else if (!validator.isEmail(values.email))
						errors.email = 'Invalid Email'

					if (!values.password) errors.password = 'Required'

					return errors
				}}
				onSubmit={({ email, password }, { resetForm }) => {
					updateEmail({
						variables: {
							email,
							password,
						},
					})

					resetForm({ values: initialValues })
				}}
			>
				{({ handleSubmit }) => (
					<form onSubmit={handleSubmit}>
						{loading && <AjaxProgress />}

						<Field name="email">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="email"
									label="Email"
									fullWidth
									required
								/>
							)}
						</Field>

						<Field name="password">
							{({ field, form }) => (
								<TextField
									field={field}
									form={form}
									type="password"
									label="Current Password"
									fullWidth
									required
								/>
							)}
						</Field>

						<Button
							type="submit"
							fullWidth
							disabled={loading}
							variant="contained"
							color="primary"
						>
							Update Email
						</Button>
					</form>
				)}
			</Formik>
		</div>
	)
}
