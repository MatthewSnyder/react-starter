import { createStore, applyMiddleware, compose } from 'redux'
import { persistState } from '@redux-devtools/core'
import promiseMiddleware from 'redux-promise'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'

import axios from 'axios'
import rootReducer from '../reducer'
import DevTools from './DevTools'

const logger = createLogger()

// By default we try to read the key from ?debug_session=<key> in the address bar
function getDebugSessionKey() {
	const matches = window.location.href.match(/[?&]debug_session=([^&]+)\b/)
	return matches && matches.length ? matches[1] : null
}

export default function configureStore(apollo, initialState) {
	const enhancer = compose(
		applyMiddleware(
			promiseMiddleware,
			thunkMiddleware.withExtraArgument({ apollo, axios }),
			logger,
			reduxImmutableStateInvariant()
		),
		window.__REDUX_DEVTOOLS_EXTENSION__
			? window.__REDUX_DEVTOOLS_EXTENSION__()
			: DevTools.instrument(),
		// Optional. Lets you write ?debug_session=<key> in address bar to persist debug sessions
		persistState(getDebugSessionKey())
	)

	const store = createStore(rootReducer(), initialState, enhancer)

	if (module.hot) {
		module.hot.accept('../reducer', () => {
			const nextReducer = require('../reducer').default()
			store.replaceReducer(nextReducer)
		})
	}

	return store
}
