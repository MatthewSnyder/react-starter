import { createStore, applyMiddleware, compose } from 'redux'
import promiseMiddleware from 'redux-promise'
import thunkMiddleware from 'redux-thunk'

import axios from 'axios'
import rootReducer from '../reducer'

export default function configureStore(apollo, initialState) {
	const enhancer = compose(
		applyMiddleware(
			promiseMiddleware,
			thunkMiddleware.withExtraArgument({ apollo, axios })
		)
	)(createStore)

	const store = enhancer(rootReducer(), initialState)

	return store
}
