import { combineReducers } from 'redux'

import notifications from 'containers/Notifications/reducer'
import dialog from 'containers/Dialog/reducer'

export default () =>
	combineReducers({
		notifications,
		dialog,
	})
