import keycode from 'keycode'

import { displayErrorDialog } from './containers/Dialog'

export const isInteractiveKeyPress = (e) =>
	keycode.isEventKey(e, 'enter') || keycode.isEventKey(e, 'space')

export const apolloErrorHandler = (
	{ graphQLErrors, networkError },
	dispatch
) => {
	if (graphQLErrors && graphQLErrors.length)
		dispatch(
			displayErrorDialog({
				body: graphQLErrors[0].message,
				options: [{ text: 'Ok' }],
			})
		)

	if (networkError)
		dispatch(
			displayErrorDialog({
				body: networkError.message,
				options: [{ text: 'Ok' }],
			})
		)
}

export const trim = (copy, maxLength) => {
	return copy.length > maxLength
		? `${copy.substring(0, maxLength - 3)} ...`
		: copy
}

export const findGetParameter = (parameterName) => {
	if (__IS_SERVER__) return null

	let result = null
	let tmp = []

	window.location.search
		.substr(1)
		.split('&')
		.forEach((item) => {
			tmp = item.split('=')
			if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1])
		})
	return result
}

const parseHeaders = (rawHeaders) => {
	const headers = new Headers()
	// Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
	// https://tools.ietf.org/html/rfc7230#section-3.2
	const preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ')

	preProcessedHeaders.split(/\r?\n/).forEach((line) => {
		const parts = line.split(':')
		const key = parts.shift().trim()

		if (key) {
			const value = parts.join(':').trim()
			headers.append(key, value)
		}
	})

	return headers
}

export const uploadFetch = (url, options) =>
	new Promise((resolve, reject) => {
		const xhr = new XMLHttpRequest()

		xhr.onload = () => {
			const opts = {
				status: xhr.status,
				statusText: xhr.statusText,
				headers: parseHeaders(xhr.getAllResponseHeaders() || ''),
			}
			opts.url =
				'responseURL' in xhr
					? xhr.responseURL
					: opts.headers.get('X-Request-URL')

			const body = 'response' in xhr ? xhr.response : xhr.responseText
			resolve(new Response(body, opts))
		}

		xhr.onerror = () => {
			reject(new TypeError('Network request failed'))
		}

		xhr.ontimeout = () => {
			reject(new TypeError('Network request failed'))
		}

		// eslint-disable-next-line security/detect-non-literal-fs-filename
		xhr.open(options.method, url, true)

		Object.keys(options.headers).forEach((key) => {
			// eslint-disable-next-line security/detect-object-injection
			xhr.setRequestHeader(key, options.headers[key])
		})

		if (xhr.upload) xhr.upload.onprogress = options.onProgress

		if (options.onAbortPossible)
			options.onAbortPossible(() => {
				xhr.abort()
			})

		xhr.send(options.body)
	})
export const isElementInViewport = (el) => {
	const rect = el.getBoundingClientRect()

	return (
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom <=
			(window.innerHeight ||
				document.documentElement.clientHeight) /* or $(window).height() */ &&
		rect.right <=
			(window.innerWidth ||
				document.documentElement.clientWidth) /* or $(window).width() */
	)
}

export const isElementMostlyInViewport = (el) => {
	const rect = el.getBoundingClientRect()

	return (
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom / 1.37 <=
			(window.innerHeight ||
				document.documentElement.clientHeight) /* or $(window).height() */ &&
		rect.right <=
			(window.innerWidth ||
				document.documentElement.clientWidth) /* or $(window).width() */
	)
}

export const isElementAtAllInViewport = (el, offset = 0) => {
	const rect = el.getBoundingClientRect()

	return (
		rect.top + offset <=
			(window.innerHeight || document.documentElement.clientHeight) &&
		rect.left >= 0 &&
		rect.right <=
			(window.innerWidth ||
				document.documentElement.clientWidth) /* or $(window).width() */
	)
}

export const isElementAtAllInViewportTop = (el, offset = 0) => {
	const rect = el.getBoundingClientRect()

	return (
		rect.top + offset <=
		(window.innerHeight || document.documentElement.clientHeight)
	)
}
