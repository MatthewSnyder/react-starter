const fs = require('fs')
const { join } = require('path')
const router = require('express').Router()
const pug = require('pug')
const mongo = require('../helpers/db')
const logger = require('../helpers/logger')
const { verifyEmail, verifyPasswordReset } = require('../schema/User/util')
const apiUrlConfig = require('../config/api-url.conf.json')
const messages = require('../config/messages')

let errorTemplate

// eslint-disable-next-line security/detect-non-literal-fs-filename
fs.readFile(join(__dirname, '../src/error.pug'), (error, data) => {
	if (error) logger.error(error)
	else errorTemplate = pug.compile(data, { cache: false })
})

router.use(`${apiUrlConfig.emailVerification}/:email/:hash`, (req, res) => {
	const { email, hash } = req.params

	verifyEmail({ email, hash, mongo, req, res })
		.then(() => {
			res.status(302)
			req.session.confirmations.push(messages.user.confirmations.emailVerified)
			res.redirect('/')
		})
		.catch((error) => {
			if (error.name === 'ApiError') {
				req.session.information.push(error.message)
				res.status(302)
				res.redirect('/')
			} else {
				logger.error(error)
				res.status(500)
				res.send(
					errorTemplate({
						error: `An error occurred. Please try again later.<br/><br/>${error}`,
					})
				)
			}
		})
})

router.use(`${apiUrlConfig.passwordReset}/:email/:hash`, (req, res) => {
	const { email, hash } = req.params

	verifyPasswordReset({ email, hash, mongo, req, res })
		.then(() => {
			res.status(302)
			req.session.confirmations.push(
				messages.user.confirmations.passwordResetRequest
			)

			res.redirect(`/password-reset/${email}/${hash}`)
		})
		.catch((error) => {
			if (error.name === 'ApiError') {
				req.session.information.push(error.message)
				res.status(302)
				res.redirect('/')
			} else {
				logger.error(error)
				res.status(500)
				res.send(
					errorTemplate({
						error: `An error occurred. Please try again later.<br/><br/>${error}`,
					})
				)
			}
		})
})

module.exports = router
