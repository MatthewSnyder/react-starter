const router = require('express').Router()
const apiUrlConfig = require('../config/api-url.conf.json')

router.use(apiUrlConfig.user, require('./user'))

module.exports = router
