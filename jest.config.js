module.exports = {
	collectCoverage: true,
	coverageReporters: ['text', 'text-summary', 'html'],
	collectCoverageFrom: ['src/**/*.{js,mjs,jsx,ts,tsx}', '!src/**/__tests__/**'],
	roots: ['./src'],
	moduleNameMapper: {
		'\\.(css|less)$': 'identity-obj-proxy',
	},
	transform: {
		'\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
			'<rootDir>/fileTransformer.js',
		'\\.[jt]sx?$': 'babel-jest',
	},
	coverageThreshold: {
		global: {
			branches: 90,
			functions: 90,
			lines: 90,
			statements: 90,
		},
	},
}
