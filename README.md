# React Starter

## Getting Started

Either install Docker on your system or fulfill the following:

(Note: mail server does not currently work in Docker)

    1. Define .env file using file in the expample files section below either in root or edit docker/envs/node.env if you plan on using Docker

    2. Install node.js on your system

    3. MongoDB server with connection specified in .env file

    4. Redis server with connection specified in .env file (if it fails to connect it will fallback to memorystore for session, which will be unable to keep sessions if app restarts)

    5. If you plan on verifying user's emails when they register (REQUIRE_EMAIL_AUTHENTICATION setting in .env file example below), a mail server for <https://nodemailer.com/about/> to use (ex. postfix) with connection specified in .env file is necessary.

## Running

Make sure to follow the previous section first as it is necessary for the app to run.

First, open a terminal in this directory.

If you installed Docker:

    1. Run "docker-compose up" or, for detached mode (background), run "docker-compose up -d"

If you did not install Docker:

    1. Run "npm install".
    2. Run "npm run dev".

Finally, go to <http://localhost:3000/> in your browser.

## Production Mode

To run this app in production mode do the following.

If you're using Docker, edit docker-compose.yml file and under node -> environment change NODE_ENV to production.
Also comment out the lines under node -> volumes as indicated by the comment.

If you did not use Docker:

    1. Run "npm run build"
    2. Run "npm start"

Now you may go to <http://localhost:3000/> in your browser.

---

## Info

### Configuration Folder

#### API URL File

This file allows you to specify server endpoints for necessary server side routes such as email verification and password resetting. The actual api functions are in the routes folder and use the url configuration from the file. The base api route is set from the main app.js file using the specified base from this file. This configuration is imported in the src folder in a file with the same filename as the original to allow it to techinically be in the src directory when building. Probably a better way to do this such as pulling into Root.jsx or something and setting it as a context using React context.

#### Helmet File

This is the configuration file for the module helmet which deals with http security headers. Documentation here: <https://helmetjs.github.io/docs/>

#### Messages File

The messages config file is a central place for messages throughout the app as for quick finding and editing.

#### WebPack File

This builds out the app from es6 syntax and less module files to browser (and node for the server file) consumable files. It is highly configurable and the documentation is extensive: <https://webpack.js.org/concepts/>

### Docker Folder

Contains folders and files to configure docker containers and for them to use.

#### Docker Envs folder

Contains the standard env file for docker to build and run the node server.

#### Docker Node Logs folder

Contains the logs of the node application that runs in a docker container.

### Email Templates Folder

These contain common use email templates for common requirements of sites such as email verification and password reset verification.

### Helpers Folder

The files in here contain core logic for the sites functionality such as the database, logging, mailing and sessions.

### Schema Folder

This folder contains GraphQL Schema for providing spec for the client app and logic for resolving queries to the database. The documentation can be found here: <https://www.apollographql.com/docs/>

### "src" Folder

The src folder contains all the React client code that runs in the browser after being compiled down by webpack and babel and a "server" file that get compiled down by the server webpack configuration into node executable code that is able to do server side rendering.

---

## To Do

- Better development mode for Docker (no polling)
- AWS
- Use YUP for form validation

### AWS Prep

1. Figure out prerequisites....

---

## Example Files

### Example .env File

    # BASE DOMAIN

    DOMAIN=[DOMAIN]

    # API_HOST INCLUDING http:// or https:// (For client calls to the server)

    API_HOST=https://[DOMAIN]

    # SERVER PORT

    PORT=3000

    # FILE SYSTEM

    FILE_SYSTEM_CACHE=1 week # uses ms

    # MONGO DATABASE

    DB_HOST=127.0.0.1
    DB_PORT=55523
    DB_DATABASE=[MONGO_DB]
    DB_USER=[MONGO_USER]
    DB_PASSWORD=[MONGO_PASSWORD]
    DB_POOL_SIZE=10

    # SESSION

    SESSION_SECRET="session secret"
    JWT_SECRET="jwt secret"
    SESSION_EXPIRE=30 days # uses ms

    # USER

    REQUIRE_EMAIL_AUTHENTICATION=true

    # REDIS SESSION

    REDIS_SESSION_HOST=127.0.0.1
    REDIS_SESSION_PORT=6379
    REDIS_SESSION_DB=0
    # REMEMBER TO USE STRONG PASSWORD IF NOT LOCAL!!!
    REDIS_SESSION_PASSWORD=[PASSWORD]

    # REDIS SOCKET.IO

    REDIS_SOCKET_IO_HOST=127.0.0.1
    REDIS_SOCKET_IO_PORT=6379
    REDIS_SOCKET_IO_DB=1
    # REMEMBER TO USE STRONG PASSWORD IF NOT LOCAL!!!
    REDIS_SOCKET_IO_PASSWORD=[PASSWORD]

    # REDIS APOLLO SERVER CACHE  !!! NOT USED (seems too complicated) https://github.com/apollographql/apollo-server/tree/master/packages/apollo-server-cache-redis

    REDIS_APOLLO_HOST=127.0.0.1
    REDIS_APOLLO_PORT=6379
    REDIS_APOLLO_DB=1
    # REMEMBER TO USE STRONG PASSWORD IF NOT LOCAL!!!
    REDIS_APOLLO_PASSWORD=[PASSWORD]

    # RATE LIMITING

    MAX_WRONG_ATTEMPS_BY_IP_PER_DAY=100
    MAX_CONSECUTIVE_FAILS_BY_USERNAME_AND_IP=10

    # SOCIAL LOGIN API

    FB_APP_ID=[FB_ID]
    FB_APP_SECRET=[FB_SECRET]
    FB_APP_VERSION=[FB_APP_VERSION]
    GOOGLE_CLIENT_ID=[GOOGLE_CLIENT_ID]

    # MAIL

    MAIL_HOST=127.0.0.1
    MAIL_PORT=25
    MAIL_SECURE=true
    MAIL_USER=[OPTIONAL_MAIL_HOST_USER]
    MAIL_PASS=[OPTIONAL_MAIL_HOST_PASSWORD]

    ERROR_CONTACT_EMAIL=[ERROR_EMAIL]

#### You can also create environment specific dotenv files such as development.env which will be picked up when your NODE_ENV environment variable is set to development, etc

### Example Unit File

    [Unit]
    Description=[SERVICE_IDENTIFIER]
    After=network.target

    [Service]
    ExecStart=/bin/sh -c 'node [ABSOLUTE_PATH_TO_PROJECT]/app.js >> [ABSOLUTE_PATH_TO_PROJECT]/logs/service-out.log 2>&1'
    Restart=always
    User=[user]
    Group=[group]
    SyslogIdentifier=[SERVICE_IDENTIFIER]
    Environment=PATH=/usr/bin:/usr/local/bin:[PATH_TO_NODE]bin/
    Environment=NODE_ENV=development PORT=3000
    WorkingDirectory=[ABSOLUTE_PATH_TO_PROJECT]

    [Install]
    WantedBy=multi-user.target

### Example Logrotate File

    [ABSOLUTE_PATH_TO_PROJECT]/logs/access.log [ABSOLUTE_PATH_TO_PROJECT]/logs/out.log [ABSOLUTE_PATH_TO_PROJECT]/logs/error.log  [ABSOLUTE_PATH_TO_PROJECT]/logs/service-out.log {
        rotate 30
        daily
        size 100k
        copytruncate
        compress
        missingok
        notifempty
        dateext
    }
