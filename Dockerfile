FROM node:17-alpine
RUN apk add inotify-tools

# Copy files as a non-root user. The `node` user is built in the Node image.
WORKDIR /app
#RUN chown node:node ./
#USER node

COPY package.json package-lock.json /app/
RUN npm install
COPY . /app/
COPY ./docker/node-envs/* .

CMD if [ ${NODE_ENV} = production ]; \
  then \
  npm run build && \
  npm start; \
  else \
  npm run dev; \
  fi

# CMD ["npm", "start"]

# Docker Windows file: C:\Users\Public\Documents\Hyper-V\Virtual Hard Disks\DockerDesktop.vhdx