/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */

const dotenv = require('dotenv')
const { join } = require('path')

const NODE_ENV = process.env.NODE_ENV || 'development'

const IS_DEV = NODE_ENV === 'development'
const IS_PROD = NODE_ENV === 'production'

dotenv.config({
	path: join('.', `${NODE_ENV}.env`),
	silent: true,
})

dotenv.config()

const { env } = process

const http = require('http')
const express = require('express')
const helmet = require('helmet')
const fs = require('fs')
const morgan = require('morgan')
const cookieParser = require('cookie-parser')
const { ApolloServer } = require('apollo-server-express')
const { makeExecutableSchema } = require('@graphql-tools/schema')
const bodyParser = require('body-parser')
const DataLoader = require('dataloader')
const url = require('url')
const ms = require('ms')
const open = require('open')

const { SESSION_EXPIRE } = require('./helpers/vars')

const mongo = require('./helpers/db')
const logger = require('./helpers/logger')

process.once('uncaughtException', (err) => {
	logger.error(err.toString())
	setTimeout(process.exit.bind(process, 1), 1000)
})

const app = express()
const port = env.PORT || 3000

app.set('port', port)
app.enable('trust proxy', 'loopback')

function serverReady() {
	const server = http.createServer(app)

	server.on('error', (error) => {
		if (error.syscall !== 'listen') throw error

		const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`

		switch (error.code) {
			case 'EACCES':
				logger.error(`${bind} requires elevated privileges`)
				process.exit(1)
				break
			case 'EADDRINUSE':
				logger.error(`${bind} is already in use`)
				process.exit(1)
				break
			default:
				throw error
		}
	})

	server.listen(port, () => logger.info('Server started', server.address()))

	if (IS_DEV) open(`http://${env.DOMAIN}:${port}`)
}

const helmetConfig = require('./config/helmet.conf.json')

app.use(
	helmet({
		...helmetConfig,
		/* hsts: { // nginx is covering this for us
			maxAge: 60 * 60 * 24 * 7 * 18 // 18 weeks min for chrome prefetching
		} */
	})
)

if (IS_DEV) app.use(require('nocache')())

app.use(require('compression')())

if (IS_PROD) {
	// create a write stream (in append mode)
	// eslint-disable-next-line security/detect-non-literal-fs-filename
	const accessLogStream = fs.createWriteStream(`${__dirname}/logs/access.log`, {
		flags: 'a',
	})

	morgan.token('date', () => new Date().toLocaleString())

	app.use(
		morgan('combined', {
			stream: accessLogStream,
			skip: (req, res) => {
				const { pathname } = url.parse(req.url)

				const styleTest = /\.(css|less|css\.map)$/
				const scriptTest = /\.(js|jsx|babel|js\.map)$/
				const imageTest = /\.(jpe?g|png|gif)$/
				const vendorTest = /vendor\//

				return (
					(styleTest.test(pathname) ||
						scriptTest.test(pathname) ||
						imageTest.test(pathname) ||
						vendorTest.test(pathname)) &&
					(res.statusCode === 200 || res.statusCode === 304)
				)
			},
		})
	)
}

app.use(bodyParser.json())

app.use(
	bodyParser.urlencoded({
		extended: false,
	})
)

if (!env.SESSION_SECRET)
	logger.warn('No session secret specified in a .env file')

app.use(
	cookieParser(env.SESSION_SECRET || 'secret', {
		maxAge: SESSION_EXPIRE,
		secure: IS_PROD,
	})
)

let webpackConfig

// eslint-disable-next-line import/no-unresolved
if (IS_PROD) webpackConfig = require('./build-info/webpack-prod-config.json')
else webpackConfig = require('./config/webpack.config')

const clientConfig = webpackConfig.find((config) => config.name === 'client')
const serverConfig = webpackConfig.find((config) => config.name === 'server')
const devServerConfig = clientConfig.devServer
const devMiddlewareConfig = devServerConfig.devMiddleware

const prodStatsPath = join(__dirname, '/build-info/webpack-prod-stats.json')

const publicFileOptions = {
	maxAge: ms(parseInt(env.FILE_SYSTEM_CACHE, 10) || IS_PROD ? '1 week' : 0),
}

if (IS_PROD)
	app.use(
		clientConfig.output.publicPath,
		express.static(clientConfig.output.path, publicFileOptions)
	)
else
	app.use(
		devMiddlewareConfig.publicPath,
		express.static(devServerConfig.contentBase, publicFileOptions)
	)

app.use(express.static(join(__dirname, 'public'), publicFileOptions))
app.use(express.static(join(__dirname, 'public/favicons'), publicFileOptions))

let compiler
let webpackDevMiddlewareInstance

if (IS_DEV) {
	compiler = require('webpack')(webpackConfig)

	webpackDevMiddlewareInstance = require('webpack-dev-middleware')(
		compiler,
		devMiddlewareConfig
	)
}

const sessionReady = async () => {
	app.use(require('./config/api-url.conf.json').api, require('./routes/api'))

	const schema = makeExecutableSchema(require('./schema'))
	const userLoader = require('./schema/User/loader')

	const getUserLoader = () =>
		new DataLoader(userLoader, { cacheKeyFn: (key) => key.toString() })

	const plugins = []

	if (IS_DEV) {
		const {
			ApolloServerPluginLandingPageGraphQLPlayground,
		} = require('apollo-server-core')

		plugins.push(ApolloServerPluginLandingPageGraphQLPlayground())
	} else if (IS_PROD) {
		const {
			ApolloServerPluginLandingPageDisabled,
		} = require('apollo-server-core')

		plugins.push(ApolloServerPluginLandingPageDisabled())
	}

	const apolloServer = new ApolloServer({
		context: ({ req, res }) => ({
			req,
			res,
			mongo,
			currentUser: req.session ? req.session.user : false,
			userLoader: getUserLoader(),
		}),
		schema,
		plugins,
	})

	await apolloServer.start()

	apolloServer.applyMiddleware({ app })

	if (IS_DEV) {
		app.use(webpackDevMiddlewareInstance)

		if (devServerConfig.hot) {
			app.use(
				require('webpack-hot-middleware')(
					compiler.compilers.find((c) => c.name === 'client'),
					{
						path: '/__webpack_hmr',
						heartbeat: 10000,
					}
				)
			)

			app.use(require('webpack-hot-server-middleware')(compiler))
		}

		webpackDevMiddlewareInstance.waitUntilValid(serverReady)

		// eslint-disable-next-line security/detect-non-literal-fs-filename
	} else if (!fs.existsSync(prodStatsPath)) {
		logger.error(`Stats file not found! ${prodStatsPath}`)
		setTimeout(() => process.exit(), 1000)
	} else {
		/* eslint-disable security/detect-non-literal-require, import/no-dynamic-require */
		const clientStats = require(prodStatsPath).children[0]

		const serverRender = require(join(
			serverConfig.output.path,
			serverConfig.output.filename
		)).default
		/* eslint-enable import/no-dynamic-require, security/detect-non-literal-require */

		app.use(serverRender({ clientStats }))

		serverReady()
	}
}

;(async () => {
	app.use(await require('./helpers/session'))
	sessionReady()
})()
